﻿namespace RemoteLock.Exporters
{
    public class ReportExportModel
    {
        public string name { get; set; }
        public string[] headerStrings { get; set; }
        public string[][] data { get; set; }
    }
}