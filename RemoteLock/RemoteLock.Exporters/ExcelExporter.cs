using System;
using ExpertXls.ExcelLib;

namespace RemoteLock.Exporters
{
    public static  class ExcelExporter
    {
        public static byte[] GetXlsxReport(ReportExportModel model)
        {
            try
            {
                var workbook = new ExcelWorkbook(ExcelWorkbookFormat.Xls_2003)
                {
                    LicenseKey = "aUJbSVtbSVhZWVtJUUdZSVpYR1hbR1BQUFA="
                };

                workbook.DocumentProperties.Title = model.name;

                workbook.Worksheets[0].Name = model.name;
                workbook.Worksheets[0].PageSetup.PaperSize = ExcelPagePaperSize.PaperA4;
                workbook.Worksheets[0].PageSetup.Orientation = ExcelPageOrientation.Landscape;
                workbook.Worksheets[0].PageSetup.LeftMargin = 1;
                workbook.Worksheets[0].PageSetup.RightMargin = 1;
                workbook.Worksheets[0].PageSetup.TopMargin = 1;
                workbook.Worksheets[0].PageSetup.BottomMargin = 1;

                var startRow = 1;

                for (var index = 0; index < model.headerStrings.Length; index++)
                {
                    var cell = workbook.Worksheets[0][startRow, index + 1];
                    cell.Text = model.headerStrings[index];
                    cell.Style.Font.Bold = true;
                }

                startRow++;

                for (var dataRow = 0; dataRow < model.data.Length; dataRow++)
                {
                    var row = model.data[dataRow];

                    for (var dataCol = 0; dataCol < model.data[dataRow].Length; dataCol++)
                        workbook.Worksheets[0][startRow + dataRow, dataCol + 1].Text = row[dataCol];
                }

                workbook.Worksheets[0].AutofitColumns();

                return workbook.Save();
            }
            catch (Exception exception)
            {
                return null;
            }
        }
    }
}