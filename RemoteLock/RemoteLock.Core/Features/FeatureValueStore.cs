using Abp.Application.Features;
using RemoteLock.Authorization.Roles;
using RemoteLock.MultiTenancy;
using RemoteLock.Users;

namespace RemoteLock.Features
{
    public class FeatureValueStore : AbpFeatureValueStore<Tenant, Role, User>
    {
        public FeatureValueStore(TenantManager tenantManager)
            : base(tenantManager)
        {
        }
    }
}