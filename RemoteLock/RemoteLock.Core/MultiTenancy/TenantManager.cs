﻿using Abp.Domain.Repositories;
using Abp.MultiTenancy;
using RemoteLock.Authorization.Roles;
using RemoteLock.Editions;
using RemoteLock.Users;

namespace RemoteLock.MultiTenancy
{
    public class TenantManager : AbpTenantManager<Tenant, Role, User>
    {
        public TenantManager(
            IRepository<Tenant> tenantRepository, 
            IRepository<TenantFeatureSetting, long> tenantFeatureRepository, 
            EditionManager editionManager) 
            : base(
                tenantRepository, 
                tenantFeatureRepository, 
                editionManager
            )
        {
        }
    }
}