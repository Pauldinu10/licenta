using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace RemoteLock.Schedule
{
    [Table("AppSchedules")]
    public class Schedule : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [Required]
        public DateTime Start { get; protected set; }
        [Required]
        public DateTime End { get; set; }
        public virtual string Text { get; set; }
        public string Description { get; set; }
        public bool Canceled { get; set; }
        public string CancelReason { get; set; }
        public int UpdateCount { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public virtual Guid? RoomId { get; set; }

        protected Schedule()
        {

        }

        public static Schedule Create(int tenantId, Guid roomId, DateTime startDate, DateTime endDate,string name, string email,string notes, string phone)
        {
            var schedule = new Schedule
            {
                Id = Guid.NewGuid(),
                TenantId = tenantId,
                RoomId = roomId,
                Start = startDate,
                End = endDate,
                Name = name,
                Email = email,
                Description = notes,
                Phone = phone
            };

            return schedule;
        }
    }
}