using System;
using System.Collections.Generic;

namespace RemoteLock.Schedule
{
    public class IntervalDto
    {
        public DateTime Start { get; set; }
        public List<int> CarIds { get; set; }
        public DateTime End { get; set; }
    }
}