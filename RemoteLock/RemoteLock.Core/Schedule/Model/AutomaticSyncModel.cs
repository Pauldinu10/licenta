﻿using System;

namespace RemoteLock.Schedule.Model
{
    public class AutomaticSyncModel
    {
        public bool IsEnabeld { get; set; }
        public DateTime? NextRun { get; set; }
        public string Recurrence { get; set; }
    }
}
