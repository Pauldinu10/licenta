using System;
using Abp.Notifications;

namespace RemoteLock.Schedule.Notifications
{
    [Serializable]
    public class SendScheduledNotificationData : NotificationData
    {
        public const string ScheduledNotificationName = "SendScheduledNotification";
        public string CategoryId { get; set; }
        public long UserId { get; set; }
        public string LocationId { get; set; }

        public SendScheduledNotificationData(string categoryId, string locationId, long userId)
        {
            CategoryId = categoryId;
            LocationId = locationId;
            UserId = userId;
        }
    }
}