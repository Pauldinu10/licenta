using System;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using Abp.Localization;
using Abp.Notifications;
using Castle.Core.Logging;
using RemoteLock.Users;

namespace RemoteLock.Schedule.Notifications
{
    public class NotifyScheduleUpdate :
        IEventHandler<EntityUpdatedEventData<Schedule>>,
        ITransientDependency
    {
        public ILogger Logger { get; set; }

        private readonly UserManager _userManager;
        private readonly INotificationPublisher _notiticationPublisher;
        private readonly ILocalizationManager _localization;
        private readonly IScheduleManager _scheduleManager;

        public NotifyScheduleUpdate(
            UserManager userManager,
            INotificationPublisher notiticationPublisher,
            ILocalizationManager localization, IScheduleManager scheduleManager)
        {
            _userManager = userManager;
            _notiticationPublisher = notiticationPublisher;
            _localization = localization;
            _scheduleManager = scheduleManager;

            Logger = NullLogger.Instance;
        }

        private string L(string key)
        {
            return _localization.GetString(RemoteLockConsts.LocalizationSourceName, key);
        }

        [UnitOfWork]
        public void HandleEvent(EntityUpdatedEventData<Schedule> eventData)
        {
        }
    }
}