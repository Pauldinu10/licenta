using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Events.Bus;
using Abp.UI;

namespace RemoteLock.Schedule
{
    public class ScheduleManager : IScheduleManager
    {
        public IEventBus EventBus { get; set; }

        private readonly IRepository<Schedule, Guid> _scheduleRepository;
        private readonly IRepository<Room.Room, Guid> _roomRepository;

        public ScheduleManager(
            IRepository<Schedule, Guid> scheduleRepository,
                IRepository<Room.Room, Guid> roomRepository
            )
        {
            _scheduleRepository = scheduleRepository;
            _roomRepository = roomRepository;

            EventBus = NullEventBus.Instance;
        }

        public async Task<Schedule> GetAsync(Guid id)
        {
            var schedule = await _scheduleRepository.FirstOrDefaultAsync(id);
            if (schedule == null)
            {
                throw new UserFriendlyException("Could not found the event, maybe it's deleted!");
            }

            return schedule;
        }

        public async Task<List<Schedule>> GetAllAsync(DateTime startDate, DateTime endDate)
        {
            return await _scheduleRepository.GetAllListAsync(item => item.Start >= startDate && item.Start < endDate ||
                                                                      item.End > startDate && item.End <= endDate);
        }

        public async Task<bool> CancelSchedule(Guid scheduleId,string reason, bool recreateIfPossible)
        {
            var schedule = await _scheduleRepository.GetAsync(scheduleId);
            schedule.Canceled = true;
            schedule.CancelReason = reason;
            schedule.UpdateCount++;

            await _scheduleRepository.UpdateAsync(schedule);

            return true;
        }

        public async Task<bool> ChangeSchedule(Guid scheduleId, Guid roomId)
        {
            var schedule = await _scheduleRepository.GetAsync(scheduleId);
            var room = await _roomRepository.GetAsync(roomId);

            schedule.RoomId = room.Id;
            schedule.UpdateCount++;

            await _scheduleRepository.UpdateAsync(schedule);

            return true;
        }


        public async Task CreateAsync(Schedule schedule)
        {
            await _scheduleRepository.InsertAsync(schedule);
        }

        public async Task<List<Guid>> GetAvailableRooms()
        {
            return (await _roomRepository.GetAllListAsync()).Select(item => item.Id).ToList();
        }

        public async Task<List<Guid>> GetAvailableRoomForScheduleChange(int tenantId,DateTime starDate, DateTime endDate)
        {
            var rooms = await GetAvailableRooms();

            var overlappingSchedulesRoomIds =
                await
                    _scheduleRepository.GetAll()
                        .Where(
                            item => item.TenantId == tenantId && !(item.Start >= endDate || item.End <= starDate) && !item.Canceled && item.RoomId.HasValue)
                        .Select(item => item.RoomId).Distinct().ToListAsync();

            foreach (var roomId in overlappingSchedulesRoomIds.Where(roomId => rooms.Contains(roomId.Value)))
                rooms.Remove(roomId.Value);

            return rooms;
        }
    }
}