using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Services;

namespace RemoteLock.Schedule
{
    public interface IScheduleManager : IDomainService
    {
        Task<Schedule> GetAsync(Guid id);
        Task<List<Schedule>> GetAllAsync(DateTime startDate, DateTime endDate);
        Task CreateAsync(Schedule schedule);

        Task<bool> CancelSchedule(Guid scheduleId,string reason, bool recreateIfPossible);
        Task<bool> ChangeSchedule(Guid scheduleId, Guid roomId);

        Task<List<Guid>> GetAvailableRoomForScheduleChange(int tenantId, DateTime starDate, DateTime endDate);
    }
}