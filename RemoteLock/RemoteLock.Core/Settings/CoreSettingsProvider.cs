﻿using System.Collections.Generic;
using Abp.Configuration;

namespace RemoteLock.Settings
{
    public class CoreSettingsProvider : SettingProvider
    {
        public const string DateTimeSettingsFormat = "yyyy-MM-dd HH:mm:ss.fff";
        public const string SettingCompanyName = "SettingCompanyName";
        public const string SettingCompanyPhone = "SettingCompanyPhone";
        public const string SettingCompanyEmail = "SettingCompanyEmail";

        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            return new[]
            {
                new SettingDefinition(
                    SettingCompanyName,
                    "",
                    scopes: SettingScopes.Tenant
                    ),

                new SettingDefinition(
                    SettingCompanyPhone,
                    "",
                    scopes: SettingScopes.Tenant
                    ),

                new SettingDefinition(
                    SettingCompanyEmail,
                    "",
                    scopes: SettingScopes.Tenant
                    )
            };
        }
    }
}
