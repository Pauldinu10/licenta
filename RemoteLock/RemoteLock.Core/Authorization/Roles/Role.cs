﻿using Abp.Authorization.Roles;
using RemoteLock.MultiTenancy;
using RemoteLock.Users;

namespace RemoteLock.Authorization.Roles
{
    public class Role : AbpRole<Tenant, User>
    {

    }
}