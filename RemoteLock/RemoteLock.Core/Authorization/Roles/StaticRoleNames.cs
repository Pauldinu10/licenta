namespace RemoteLock.Authorization.Roles
{
    public static class StaticRoleNames
    {
        public static class Host
        {
            public const string Admin = "Admin";
        }

        public static class Tenants
        {
            public const string Admin = "Admin";
            public const string Manager = "Manager";
            public const string Member = "Member";

            public const string ORMManager = "ORMManager";
            public const string ORMMember = "ORMMember";
        }
    }
}