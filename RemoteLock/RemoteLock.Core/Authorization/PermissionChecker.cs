﻿using Abp.Authorization;
using RemoteLock.Authorization.Roles;
using RemoteLock.MultiTenancy;
using RemoteLock.Users;

namespace RemoteLock.Authorization
{
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
