﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace RemoteLock.Authorization
{
    public class RemoteLockAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //Common permissions
            var pages = context.GetPermissionOrNull(PermissionNames.Pages) ??
                        context.CreatePermission(PermissionNames.Pages, L("Pages"),
                            multiTenancySides: MultiTenancySides.Tenant);

            var management = context.GetPermissionOrNull(PermissionNames.Management) ??
                                        context.CreatePermission(PermissionNames.Management, L("Management"),
                                            multiTenancySides: MultiTenancySides.Tenant);

            var reports = context.GetPermissionOrNull(PermissionNames.Reports) ??
                                 context.CreatePermission(PermissionNames.Reports, L("Reports"),
                                     multiTenancySides: MultiTenancySides.Tenant);

            var settings = context.GetPermissionOrNull(PermissionNames.Settings) ??
                                 context.CreatePermission(PermissionNames.Settings, L("Settings"),
                                     multiTenancySides: MultiTenancySides.Tenant);

            var orm_pages = context.GetPermissionOrNull(PermissionNames.ORM_Pages) ??
                                 context.CreatePermission(PermissionNames.ORM_Pages, L("ORM_Pages"),
                                     multiTenancySides: MultiTenancySides.Tenant);

            var orm_reports = context.GetPermissionOrNull(PermissionNames.ORM_Reports) ??
                                 context.CreatePermission(PermissionNames.ORM_Reports, L("ORM_Reports"),
                                     multiTenancySides: MultiTenancySides.Tenant);

            //Host permissions
            var tenants = pages.CreateChildPermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Tenant);

            var role_management = context.GetPermissionOrNull(PermissionNames.RoleManagment) ??
                                 context.CreatePermission(PermissionNames.RoleManagment, L("RoleManagment"),
                                     multiTenancySides: MultiTenancySides.Tenant);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, RemoteLockConsts.LocalizationSourceName);
        }
    }
}
