﻿namespace RemoteLock.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants"; //grants tenant management permission
        public const string Pages = "Pages"; //grants organizer permissions calendar, check in/check out
        public const string Management = "Management"; // grants organization user, locations,categories, vehicles and sync settings permission
        public const string Reports = "Reports"; // grants schedule reports permission
        public const string Settings = "Settings"; // grants schedule settings permission

        public const string ORM_Reports = "ORM.Reports"; // grants orm reports permision
        public const string ORM_Pages = "ORM.Pages"; // grants orm pages permission

        public const string RoleManagment = "RoleManagement";//grants or revoke permissions to roles
    }
}