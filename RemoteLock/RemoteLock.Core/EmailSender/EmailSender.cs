﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace RemoteLock.EmailSender
{
    public class EmailModel
    {
        public string EmailAddress { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public bool IsBodyHtml { get; set; }
        public MemoryStream AttachmentStream { get; set; }
        public string AttachmentTitle { get; set; }
    }

    public static class EmailSender
    {

        public const string PrefixLink = "http://localhost";

        public static void  SendEmailUsingGmail(EmailModel model)
        {
            try
            {
                using (var message = new MailMessage { Body = model.Body })
                {
                    message.To.Add(model.EmailAddress);
                    message.From = new MailAddress("paul@orangefresh.ro", "Remote Lock", Encoding.UTF8);

                    message.BodyEncoding = Encoding.UTF8;
                    message.Subject = model.Subject;
                    message.SubjectEncoding = Encoding.UTF8;
                    message.IsBodyHtml = model.IsBodyHtml;

                    if (model.AttachmentStream != null)
                        message.Attachments.Add(new Attachment(model.AttachmentStream, model.AttachmentTitle));

                    using (var client = new SmtpClient("smtp.office365.com", 587)
                    {
                        Credentials = new NetworkCredential("paul@orangefresh.ro", "Driver123"),
                        EnableSsl = true
                    })
                    {

                        client.Send(message);

                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public static void SendEmail(EmailModel model)
        {
            SendEmailUsingGmail(model);
        }
    }
}
