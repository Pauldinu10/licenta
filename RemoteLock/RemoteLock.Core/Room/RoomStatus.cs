﻿namespace RemoteLock.Room
{
    public enum RoomStatus
    {
        Free = 0,
        Occupied = 1,
        NeedsCleanning = 2,
        NeedsRepairs = 3,
        OutOfService = 4
    }
}