﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace RemoteLock.Room
{
    public class RoomSecurity
    {
        public static string GetCode(string deviceId, DateTime dateTime)
        {
            var byteList = deviceId.Split('-')
                .Select(hex => Convert.ToByte(hex, 16))
                .ToList();

            var dateByteList = new List<byte>
            {
                Convert.ToByte(dateTime.Day),
                Convert.ToByte(dateTime.Month),
                Convert.ToByte(dateTime.Year % 100)
            };

            byteList.AddRange(dateByteList);

            var buffer = byteList.ToArray();

            ushort todayCodeSuffix = Scramble(buffer, 9);

            byte todayCodePrefix = compute(todayCodeSuffix, buffer[6]);

            var result =
                todayCodePrefix.ToString()+todayCodeSuffix.ToString().PadLeft(5, '0');

            return result;
        }

        static ushort Scramble(byte[] buffer, int bufferLen)
        {
            ushort codEx = 0;
            for (var j = 0; j < bufferLen; j++)
            {
                codEx = (ushort)(codEx ^ (buffer[j]));

                for (var i = 0; i <= 7; i++)
                {
                    if ((codEx & 0x6000) != 0)
                    {
                        codEx = (ushort)(codEx << 1);
                        codEx = (ushort)(codEx ^ 0x6005);
                    }
                    else
                        codEx = (ushort)(codEx << 1);
                }
            }
            return codEx;
        }

        static byte compute(ushort c, byte day)
        {
            byte cc = Convert.ToByte(Convert.ToByte(c % 256));
            if ((day & 1) == 1)
                return cc %= 13;
            else
                return cc %= 17;
            return cc;
        }
    }
}