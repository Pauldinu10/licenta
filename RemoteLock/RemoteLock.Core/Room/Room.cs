﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace RemoteLock.Room
{
    [Table("AppRooms")]
    public class Room : FullAuditedEntity<Guid>, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [Required]
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public string DeviceId { get; set; }
        public string Address { get; set; }
        public string AirbnbId { get; set; }
        public string BookingId { get; set; }
        public DateTime SyncDateTimeAirbnb { get; set; }
        public DateTime SyncDateTimeBooking { get; set; }
        public DateTime InstallDate { get; set; }
        public DateTime BatteryChangedDate { get; set; }
        public int Status { get; set; }
        protected Room()
        {

        }

        public static Room Create(int tenantId, string name,string address, string description, string deviceId, string airbnbId, string bookingId)
        {
            var company = new Room
            {
                Id = Guid.NewGuid(),
                TenantId = tenantId,
                Name = name,
                Description = description,
                DeviceId = deviceId,
                AirbnbId = airbnbId,
                BookingId = bookingId,
                Status = (int)RoomStatus.Free,
                InstallDate = DateTime.MaxValue,
                BatteryChangedDate = DateTime.MaxValue,
                SyncDateTimeAirbnb = DateTime.MaxValue,
                SyncDateTimeBooking = DateTime.MaxValue,
                Address = address
            };


            if (!string.IsNullOrEmpty(deviceId))
            {
                company.InstallDate = DateTime.Now;
                company.BatteryChangedDate = DateTime.Now;
            }

            return company;
        }
    }
}
