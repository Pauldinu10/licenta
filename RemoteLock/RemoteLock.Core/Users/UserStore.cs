using System;
using System.Threading.Tasks;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Microsoft.AspNet.Identity;
using RemoteLock.Authorization.Roles;
using RemoteLock.MultiTenancy;

namespace RemoteLock.Users
{
    public class UserStore : AbpUserStore<Tenant, Role, User>
    {
        private readonly IRepository<User, long> _userRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public const string DefaultPassword="12345";

        public UserStore(
            IRepository<User, long> userRepository,
            IRepository<UserLogin, long> userLoginRepository,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<Role> roleRepository,
            IRepository<UserPermissionSetting, long> userPermissionSettingRepository,
            IUnitOfWorkManager unitOfWorkManager)
            : base(
              userRepository,
              userLoginRepository,
              userRoleRepository,
              roleRepository,
              userPermissionSettingRepository,
              unitOfWorkManager)
        {
            _userRepository = userRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task ToggleUserSuspendedState(long id)
        {
            var user = await _userRepository.GetAsync(id);

            user.IsActive = !user.IsActive;

            await _userRepository.UpdateAsync(user);
        }

        public async Task<long> AddUser(int tenantId, string name, string surname, string email, string password, string username=null, int? type=null)
        {
            var userModel = User.CreateTenantUser(tenantId, name, surname, email, password, username, type);
            return await _userRepository.InsertOrUpdateAndGetIdAsync(userModel);
        }

        public async Task<User> UpdateUser(long userId, string name, string surname, string email,
             int? type)
        {
            var user = await _userRepository.GetAsync(userId);

            user.Name = name;
            user.Surname = surname;
            user.EmailAddress = email;
            user.UserType = type;

            return await _userRepository.UpdateAsync(user);
        }

        public async Task<bool> ChangePassword(long userId, string OldPassword, string newPassword)
        {
            var user = await _userRepository.GetAsync(userId);

            var result = new PasswordHasher().VerifyHashedPassword(user.Password, OldPassword);

            if (result == PasswordVerificationResult.Success)
            {
                user.Password = new PasswordHasher().HashPassword(newPassword);
                await _userRepository.UpdateAsync(user);

                return true;
            }

            return false;
        }

        public async Task ResetPassword(long userId)
        {
            var user = await _userRepository.GetAsync(userId);
                user.Password = new PasswordHasher().HashPassword(DefaultPassword);
                await _userRepository.UpdateAsync(user);
        }
    }
}