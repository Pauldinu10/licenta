﻿using System;
using Abp.Authorization.Users;
using Abp.Extensions;
using Microsoft.AspNet.Identity;
using RemoteLock.MultiTenancy;

namespace RemoteLock.Users
{
    public class User : AbpUser<Tenant, User>
    {
        public const string DefaultPassword = "123qwe";
        public int? UserType { get; set; }
        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public static User CreateTenantAdminUser(int tenantId, string emailAddress, string password)
        {
            return new User
            {
                TenantId = tenantId,
                UserName = AdminUserName,
                Name = AdminUserName,
                Surname = AdminUserName,
                EmailAddress = emailAddress,
                Password = new PasswordHasher().HashPassword(password),
            };
        }

        public static User CreateTenantUser(int tenantId, string name, string surname, string email, string password, string username=null,int? type=null)
        {
            return new User
            {
                TenantId = tenantId,
                UserName = username??email,
                Name = name,
                Surname = surname,
                EmailAddress = email,
                Password = new PasswordHasher().HashPassword(password),
                IsActive = true,
                UserType = type
            };
        }

        public static string bossUserUsername = "boss";
        public static User CreatebossUser(int tenantId, string password)
        {
            return new User
            {
                TenantId = tenantId,
                UserName = bossUserUsername,
                Name = bossUserUsername,
                Surname = bossUserUsername,
                EmailAddress = bossUserUsername,
                Password = password,
                IsActive = true
            };
        }

        public string GetDisplayName()
        {
            return $"{Surname} {Name}";
        }
    }
}