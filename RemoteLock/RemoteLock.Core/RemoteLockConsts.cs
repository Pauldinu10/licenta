﻿using System;

namespace RemoteLock
{
    public class RemoteLockConsts
    {
        public const string LocalizationSourceName = "RemoteLock";

        public static TimeZoneInfo EESTTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("E. Europe Standard Time");
    }
}