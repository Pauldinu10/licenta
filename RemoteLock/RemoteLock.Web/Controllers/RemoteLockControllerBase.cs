﻿using System.Web.Mvc;
using Abp.IdentityFramework;
using Abp.UI;
using Abp.Web.Mvc.Controllers;
using Microsoft.AspNet.Identity;

namespace RemoteLock.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class RemoteLockControllerBase : AbpController
    {
        protected RemoteLockControllerBase()
        {
            LocalizationSourceName = RemoteLockConsts.LocalizationSourceName;
        }

        protected virtual void CheckModelState()
        {
            if (!ModelState.IsValid)
            {
                ModelState outPass;
                if (ModelState.TryGetValue("ConfirmPassword", out outPass) && outPass.Errors.Count > 0)
                {
                    throw new UserFriendlyException(L(outPass.Errors[0].ErrorMessage));
                }

                if (ModelState.TryGetValue("ConfirmPassword", out outPass) && outPass.Errors.Count > 0)
                {
                    throw new UserFriendlyException(L(outPass.Errors[0].ErrorMessage));
                }

                throw new UserFriendlyException(L("FormIsNotValidMessage"));
            }
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}