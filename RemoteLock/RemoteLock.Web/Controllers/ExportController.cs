using System;
using System.Web.Mvc;
using RemoteLock.Exporters;

namespace RemoteLock.Web.Controllers
{
    [System.Web.Http.AllowAnonymous]
    public class ExportController : RemoteLockControllerBase
    {
        [System.Web.Http.HttpPost]
        public ActionResult ExportExcel(ReportExportModel model)
        {
            var byteArray = ExcelExporter.GetXlsxReport(model);

            string id = Guid.NewGuid().ToString();
            string path = Server.MapPath($"~/temp/{id}.tmp");

            System.IO.File.WriteAllBytes(path, byteArray);

            return Content(id);
        }

        public ActionResult Download(string id,string reportName)
        {
            string path = Server.MapPath($"~/temp/{id}.tmp");
            var byteArray = System.IO.File.ReadAllBytes(path);
            System.IO.File.Delete(path);

            Response.AddHeader("Content-Disposition", $"attachment; filename={reportName}");

            var mimeType = reportName.EndsWith(".xls") ? "application/vnd.ms-excel" : "application/pdf";

            return File(byteArray, mimeType);
        }
    }
}