﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Configuration.Startup;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using Abp.UI;
using Abp.Web.Mvc.Models;
using EntityFramework.DynamicFilters;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using RemoteLock.Authorization;
using RemoteLock.Authorization.Roles;
using RemoteLock.EmailSender;
using RemoteLock.EntityFramework;
using RemoteLock.MultiTenancy;
using RemoteLock.Schedule;
using RemoteLock.Users;
using RemoteLock.Web.Models.Account;

namespace RemoteLock.Web.Controllers
{
    public class AccountController : RemoteLockControllerBase
    {
        private readonly TenantManager _tenantManager;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IPermissionManager _permissionManager;
        private readonly IScheduleManager _scheduleManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly RemoteLockDbContext _dbRemoteDbContext;
        private readonly IMultiTenancyConfig _multiTenancyConfig;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        //public string aux =new PasswordHasher().HashPassword("test123");
        public string TenantRegistrationPass =
           // "AEaI0l/dB/D9Jcocy3IbHiDg2sqjyCZrU086fvvczKlfMIn+iooR99+nThYANvxmLg==";
           new PasswordHasher().HashPassword("pauldpauld");

        public AccountController(
            TenantManager tenantManager,
            UserManager userManager,
            RoleManager roleManager,
            IPermissionManager permissionManager,
            ScheduleManager scheduleManager,
            IUnitOfWorkManager unitOfWorkManager,
            RemoteLockDbContext dbRemoteDbContext,
            IMultiTenancyConfig multiTenancyConfig)
        {
            _tenantManager = tenantManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _permissionManager = permissionManager;
            _unitOfWorkManager = unitOfWorkManager;
            _dbRemoteDbContext = dbRemoteDbContext;
            _multiTenancyConfig = multiTenancyConfig;
            _scheduleManager = scheduleManager;
        }

        #region Login / Logout




        public ActionResult Login(string returnUrl = "")
        {
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                returnUrl = Request.ApplicationPath;
            }

            return View(
                new LoginFormViewModel
                {
                    ReturnUrl = returnUrl,
                    IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled
                });
        }

        public async Task<AbpUserManager<Tenant, Role, User>.AbpLoginResult> GetLoginResultAsync(string usernameOrEmailAddress, string password, string tenancyName)
        {
            var loginResult = await _userManager.LoginAsync(usernameOrEmailAddress, password, tenancyName);

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    return loginResult;
                default:
                    throw CreateExceptionForFailedLoginAttempt(loginResult.Result, usernameOrEmailAddress, tenancyName);
            }

            string getLoginName()
            {
            return usernameOrEmailAddress;
            }
        }
       

        private async Task SignInAsync(User user, ClaimsIdentity identity = null, bool rememberMe = false)
        {
            if (identity == null)
            {
                identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            }

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = rememberMe }, identity);
        }

        private Exception CreateExceptionForFailedLoginAttempt(AbpLoginResultType result, string usernameOrEmailAddress, string tenancyName)
        {
            switch (result)
            {
                case AbpLoginResultType.Success:
                    return new ApplicationException("Don't call this method with a success result!");
                case AbpLoginResultType.InvalidUserNameOrEmailAddress:
                case AbpLoginResultType.InvalidPassword:
                    return new UserFriendlyException(L("LoginFailed"), L("InvalidUserNameOrPassword"));
                case AbpLoginResultType.InvalidTenancyName:
                    return new UserFriendlyException(L("LoginFailed"), L("ThereIsNoTenantDefinedWithName{0}", tenancyName));
                case AbpLoginResultType.TenantIsNotActive:
                    return new UserFriendlyException(L("LoginFailed"), L("TenantIsNotActive", tenancyName));
                case AbpLoginResultType.UserIsNotActive:
                    return new UserFriendlyException(L("LoginFailed"), L("UserIsNotActiveAndCanNotLogin", usernameOrEmailAddress));
                case AbpLoginResultType.UserEmailIsNotConfirmed:
                    return new UserFriendlyException(L("LoginFailed"), "Your email address is not confirmed. You can not login"); //TODO: localize message
                default: //Can not fall to default actually. But other result types can be added in the future and we may forget to handle it
                    Logger.Warn("Unhandled login fail reason: " + result);
                    return new UserFriendlyException(L("LoginFailed"));
            }
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        #endregion

        #region Login no company

        [HttpPost]
        [DisableAuditing]
        public async Task<JsonResult> Login(LoginViewModel loginModel, string returnUrl = "", string returnUrlHash = "")
        {
            CheckModelState();

            _dbRemoteDbContext.DisableAllFilters();

            List<User> users = new List<User>();
            List<string> tenants = new List<string>();

            if (loginModel.TenancyName == null)
            {
                users =
                    await
                        _dbRemoteDbContext.Users.Where(
                            item => item.UserName == loginModel.UsernameOrEmailAddress || item.EmailAddress == loginModel.UsernameOrEmailAddress)
                            .ToListAsync();

                if (users.Count > 1)
                    users = users.Where(item => item.IsActive).ToList();

                 tenants = users.Where(u => u.TenantId != null)
                    .Select(u => AsyncHelper.RunSync(() => _tenantManager.FindByIdAsync(u.TenantId.Value)))
                    .Select(item => item.Name).ToList();
            }


            if (users.Count <= 1 || loginModel.TenancyName != null)
            {
                var loginResult = await GetLoginResultAsync(
                    loginModel.UsernameOrEmailAddress,
                    loginModel.Password, tenants.Count == 1 ? tenants[0]: loginModel.TenancyName
                    );

                await SignInAsync(loginResult.User, loginResult.Identity, loginModel.RememberMe);

                if (string.IsNullOrWhiteSpace(returnUrl))
                {
                    returnUrl = Request.ApplicationPath;
                }

                if (!string.IsNullOrWhiteSpace(returnUrlHash))
                {
                    returnUrl = returnUrl + returnUrlHash;
                }

                return Json(new MvcAjaxResponse {TargetUrl = returnUrl});
            }

            return Json(new MvcAjaxResponse {TargetUrl = returnUrl, Result = tenants});
        }

        #endregion

        #region Password Reset

        [HttpPost]
        [UnitOfWork]
        [DisableAuditing]
        public virtual async Task<JsonResult> PasswordReset(PasswordResetViewModel model)
        {
            CheckModelState();

            _dbRemoteDbContext.DisableAllFilters();

            List<User> users = new List<User>();
            List<string> tenants = new List<string>();

            if (model.TenancyName == null)
            {
                users =
                    await
                        _dbRemoteDbContext.Users.Where(
                            item => item.EmailAddress == model.EmailAddress)
                            .ToListAsync();

                tenants = users.Where(u => u.TenantId != null)
                   .Select(u => AsyncHelper.RunSync(() => _tenantManager.FindByIdAsync(u.TenantId.Value)))
                   .Select(item => item.Name).ToList();
            }


            if (users.Count <= 1 || model.TenancyName != null)
            {
                string resetCode = null;
                string emailAddress = null;
                string displayname = null;

                if (users.Count == 1)
                {
                    resetCode = GeneratePasswordResetCode();
                    users[0].PasswordResetCode = resetCode;
                    emailAddress = users[0].EmailAddress;
                    displayname = users[0].GetDisplayName();
                }
                else
                {
                    var tenantId = (await _tenantManager.FindByTenancyNameAsync(model.TenancyName)).Id;
                    var user =
                        await
                            _dbRemoteDbContext.Users.Where(
                                item => item.EmailAddress == model.EmailAddress && item.TenantId == tenantId)
                                .SingleAsync();

                    resetCode = GeneratePasswordResetCode();
                    user.PasswordResetCode = resetCode;
                    emailAddress = user.EmailAddress;
                    displayname = user.GetDisplayName();
                }

                await _dbRemoteDbContext.SaveChangesAsync();

                var tempalteModel = new PasswordResetTemplateModel
                {
                    DisplayName = displayname,
                    PasswordResetLink =
                        EmailSender.EmailSender.PrefixLink +
                        Url.Action("ConfirmPasswordReset", new {passwordResetCode = resetCode})
                };

                var emailBody = this.RenderRazorViewToString("~/Views/EmailTemplates/PasswordResetTemplate.cshtml",
                    tempalteModel);

                EmailSender.EmailSender.SendEmail(new EmailModel
                {
                    EmailAddress = emailAddress,
                    Body = emailBody,
                    IsBodyHtml = true,
                    Subject = L("Account_PasswordReset_Email_Subject")
                });

                return Json(new JsonResult {Data = true});
            }

            return Json(new JsonResult { Data = tenants });
        }

        public string GeneratePasswordResetCode()
        {
            return Guid.NewGuid().ToString();
        }

        [UnitOfWork]
        public virtual async Task<ActionResult> PasswordReset()
        {
            return View("PasswordReset", new PasswordResetViewModel());
        }

        [UnitOfWork]
        public virtual async Task<ActionResult> ConfirmPasswordReset(string passwordResetCode)
        {
            CheckModelState();

            _dbRemoteDbContext.DisableAllFilters();

            var user =
                await
                    _dbRemoteDbContext.Users.Where(
                        item => item.PasswordResetCode == passwordResetCode)
                        .FirstOrDefaultAsync();

            if (user ==null)
                return RedirectToAction("Login");

            return View("ConfirmPasswordReset", new PasswordConfirmViewModel
            {
                Username = user.UserName,
                PasswordResetCode = passwordResetCode
            });
        }

        [UnitOfWork]
        [DisableAuditing]
        [HttpPost]
        public virtual async Task<ActionResult> ChangePassword(PasswordConfirmViewModel model)
        {
            CheckModelState();

            _dbRemoteDbContext.DisableAllFilters();

            var user =
                await
                    _dbRemoteDbContext.Users.Where(
                        item => item.PasswordResetCode == model.PasswordResetCode)
                        .FirstOrDefaultAsync();
            if (user != null)
            {
                user.PasswordResetCode = null;
                user.Password = new PasswordHasher().HashPassword(model.Password);

                await _dbRemoteDbContext.SaveChangesAsync();
            }

            return RedirectToAction("Login");
        }

        #endregion

        #region Tenant Registration

        public ActionResult RegisterTenant()
        {
            return View("RegisterTenant", new RegisterTenantViewModel());
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<ActionResult> RegisterTenant(RegisterTenantViewModel model)
        {
            try
            {
                AuthenticationManager.SignOut();

                CheckModelState();

                if (new PasswordHasher().VerifyHashedPassword(TenantRegistrationPass, model.bossCreationKey) == PasswordVerificationResult.Failed)
                    throw new UserFriendlyException("Parola companiei invalida!");

                //Create tenant
                var tenant = new Tenant
                {
                    TenancyName = model.TenancyName,
                    Name = model.TenancyName,
                };

                CheckErrors(await _tenantManager.CreateAsync(tenant));
                await _unitOfWorkManager.Current.SaveChangesAsync(); //To get new tenant's id.

                //We are working entities of new tenant, so changing tenant filter
                using (_unitOfWorkManager.Current.SetFilterParameter(AbpDataFilters.MayHaveTenant, AbpDataFilters.Parameters.TenantId, tenant.Id))
                {
                    //Create static roles for new tenant
                    CheckErrors(await _roleManager.CreateStaticRoles(tenant.Id));
                    await _unitOfWorkManager.Current.SaveChangesAsync(); //To get static role ids

                    await SetupRolesAndPermissions(tenant.Id);

                    //Create client manager user for the tenant

                    var clientUser = Users.User.CreateTenantUser(tenant.Id, model.Name, model.Surname, model.EmailAddress,
                        model.Password, null, null);

                    CheckErrors(await _userManager.CreateAsync(clientUser));
                    await _unitOfWorkManager.Current.SaveChangesAsync(); //To get client user's id

                    //Assign manager user to client user!
                    CheckErrors(
                        await
                            _userManager.AddToRoleAsync(clientUser.Id,
                                model.IsORM ? StaticRoleNames.Tenants.ORMManager : StaticRoleNames.Tenants.Manager));
                    await _unitOfWorkManager.Current.SaveChangesAsync();

                    //Create boss user for the tenant with admin role
                    var bossUser = Users.User.CreatebossUser(tenant.Id, TenantRegistrationPass);

                    CheckErrors(await _userManager.CreateAsync(bossUser));
                    await _unitOfWorkManager.Current.SaveChangesAsync(); //To get admin user's id

                    //Assign boss user to role!
                    CheckErrors(await _userManager.AddToRoleAsync(bossUser.Id, StaticRoleNames.Tenants.Admin));
                    await _unitOfWorkManager.Current.SaveChangesAsync();

                    //Login!
                    var loginResult = await GetLoginResultAsync(model.EmailAddress, model.Password, tenant.TenancyName);
                    if (loginResult.Result == AbpLoginResultType.Success)
                    {
                        await SignInAsync(loginResult.User, loginResult.Identity);
                        return Redirect(Url.Action("Index", "Home"));
                    }
                }

                return RedirectToAction("Index", "Home");
            }
            catch (UserFriendlyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("RegisterTenant", model);
            }
        }

        private async Task SetupRolesAndPermissions(int tenantId)
        {
            //grant all permissions to admin role
            var adminRole =
                _roleManager.Roles.Single(
                    r => r.Name == StaticRoleNames.Tenants.Admin && r.TenantId == tenantId);
            await _roleManager.GrantAllPermissionsAsync(adminRole);


            var pagesPermission = _permissionManager.GetPermissionOrNull(PermissionNames.Pages);
            var mangementPermission = _permissionManager.GetPermissionOrNull(PermissionNames.Management);
            var reportsPermission = _permissionManager.GetPermissionOrNull(PermissionNames.Reports);
            var settingsPermission = _permissionManager.GetPermissionOrNull(PermissionNames.Settings);

            var orm_pages = _permissionManager.GetPermissionOrNull(PermissionNames.ORM_Pages);
            var orm_reports = _permissionManager.GetPermissionOrNull(PermissionNames.ORM_Reports);

            await _roleManager.ProhibitPermissionAsync(adminRole, pagesPermission);
            await _roleManager.ProhibitPermissionAsync(adminRole, orm_pages);

            //grant pages permissions to member role
            var memberRole =
                _roleManager.Roles.Single(
                    r => r.Name == StaticRoleNames.Tenants.Member && r.TenantId == tenantId);

            if (memberRole != null)
                await _roleManager.GrantPermissionAsync(memberRole, pagesPermission);

            //grant pages,managment,reports, settings permissions to management role
            var mangementRole =
                _roleManager.Roles.Single(
                    r => r.Name == StaticRoleNames.Tenants.Manager && r.TenantId == tenantId);

            if (mangementPermission != null)
            {
                await _roleManager.GrantPermissionAsync(mangementRole, pagesPermission);
                await _roleManager.GrantPermissionAsync(mangementRole, mangementPermission);
                await _roleManager.GrantPermissionAsync(mangementRole, reportsPermission);
                await _roleManager.GrantPermissionAsync(mangementRole, settingsPermission);
            }

            //grant orm_pages permissions to orm_member role
            var orm_memberRole = 
                _roleManager.Roles.Single(
                    r => r.Name == StaticRoleNames.Tenants.ORMMember && r.TenantId == tenantId);

            if (orm_memberRole != null)
                await _roleManager.GrantPermissionAsync(orm_memberRole, orm_pages);

            //grant orm_pages managment,orm_reports to orm_manager role
            var orm_mangementRole = 
                _roleManager.Roles.Single(
                    r => r.Name == StaticRoleNames.Tenants.ORMManager && r.TenantId == tenantId);

            if (orm_mangementRole != null)
            {
                await _roleManager.GrantPermissionAsync(orm_mangementRole, orm_pages);
                await _roleManager.GrantPermissionAsync(orm_mangementRole, mangementPermission);
                await _roleManager.GrantPermissionAsync(orm_mangementRole, orm_reports);
            }
        }

#endregion


#region Common private methods

        private async Task<Tenant> GetActiveTenantAsync(string tenancyName)
        {
            var tenant = await _tenantManager.FindByTenancyNameAsync(tenancyName);
            if (tenant == null)
            {
                throw new UserFriendlyException(L("ThereIsNoTenantDefinedWithName{0}", tenancyName));
            }

            if (!tenant.IsActive)
            {
                throw new UserFriendlyException(L("TenantIsNotActive", tenancyName));
            }

            return tenant;
        }

#endregion

#region Cleanup

        [HttpGet]
        [UnitOfWork]
        public virtual async Task<ActionResult> RestructureRolesAndPermission(string bossCreationKey)
        {
            AuthenticationManager.SignOut();

            if (new PasswordHasher().VerifyHashedPassword(TenantRegistrationPass, bossCreationKey) == PasswordVerificationResult.Failed) 
                throw new UserFriendlyException("Parola super user invalida!");

            try
            {
                var tenants = await _tenantManager.Tenants.ToListAsync();

                foreach (var tenant in tenants)
                {
                    if (tenant.Name == "Default") continue;

                    //We are working entities of new tenant, so changing tenant filter
                    using (
                        _unitOfWorkManager.Current.SetFilterParameter(AbpDataFilters.MayHaveTenant,
                            AbpDataFilters.Parameters.TenantId, tenant.Id))
                    {
                        //add new roles
                        var orm_memberRole =
                            _roleManager.Roles.FirstOrDefault(
                                r => r.Name == StaticRoleNames.Tenants.ORMMember && r.TenantId == tenant.Id);

                        if (orm_memberRole == null)
                            CheckErrors(await _roleManager.CreateAsync(new Role
                            {
                                TenantId = tenant.Id,
                                Name = StaticRoleNames.Tenants.ORMMember,
                                DisplayName = StaticRoleNames.Tenants.ORMMember,
                                IsDefault = false,
                                IsStatic = true,
                                IsDeleted = false,
                                CreationTime = DateTime.UtcNow
                            }));

                        var orm_managerRole =
                            _roleManager.Roles.FirstOrDefault(
                                r => r.Name == StaticRoleNames.Tenants.ORMManager && r.TenantId == tenant.Id);

                        if (orm_managerRole == null)
                            CheckErrors(await _roleManager.CreateAsync(new Role
                            {
                                TenantId = tenant.Id,
                                Name = StaticRoleNames.Tenants.ORMManager,
                                DisplayName = StaticRoleNames.Tenants.ORMManager,
                                IsDefault = false,
                                IsStatic = true,
                                IsDeleted = false,
                                CreationTime = DateTime.UtcNow
                            }));

                        await _unitOfWorkManager.Current.SaveChangesAsync(); //To get static role ids

                        await SetupRolesAndPermissions(tenant.Id);

                        var isbossUserCreated = false;

                        foreach (var user in _userManager.Users.ToList())
                        {
                            if (user.UserName == Users.User.bossUserUsername)
                            {
                                isbossUserCreated = true;
                                continue;
                            }

                            var roles = await _userManager.GetRolesAsync(user.Id);

                            if (roles.Any(item => item == StaticRoleNames.Tenants.Admin))
                            {
                                await _userManager.RemoveFromRoleAsync(user.Id, StaticRoleNames.Tenants.Admin);
                                await _userManager.AddToRolesAsync(user.Id, StaticRoleNames.Tenants.Manager);
                            }
                            else
                            {
                                if (roles.Count==0)
                                {
                                    await _userManager.AddToRolesAsync(user.Id, StaticRoleNames.Tenants.Member);
                                }
                            }

                            await _unitOfWorkManager.Current.SaveChangesAsync();

                        }

                        if (!isbossUserCreated)
                        {
                            //Create boss user for the tenant with admin role
                            var bossUser = Users.User.CreatebossUser(tenant.Id, TenantRegistrationPass);

                            CheckErrors(await _userManager.CreateAsync(bossUser));
                            await _unitOfWorkManager.Current.SaveChangesAsync(); //To get admin user's id)

                            //Assign boss user to role!
                            CheckErrors(await _userManager.AddToRoleAsync(bossUser.Id, StaticRoleNames.Tenants.Admin));
                            await _unitOfWorkManager.Current.SaveChangesAsync();
                        }
                    }
                }

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
#endregion
    }
}