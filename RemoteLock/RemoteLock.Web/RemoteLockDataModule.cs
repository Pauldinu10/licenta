﻿using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;

namespace RemoteLock
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(RemoteLockCoreModule))]
    public class RemoteLockDataModule : AbpModule
    {
        public override void PreInitialize()
        {

            Configuration.DefaultNameOrConnectionString = "Default";

        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
