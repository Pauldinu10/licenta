﻿using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using RemoteLock.Authorization.Roles;
using RemoteLock.MultiTenancy;
using RemoteLock.Users;

namespace RemoteLock.EntityFramework
{
    public class RemoteLockDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        public virtual IDbSet<Schedule.Schedule> Schedules { get; set; }
        public virtual IDbSet<Room.Room> Rooms { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public RemoteLockDbContext()

            : base("Default")

        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in RemoteLockDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of RemoteLockDbContext since ABP automatically handles it.
         */
        public RemoteLockDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public RemoteLockDbContext(DbConnection connection)
            : base(connection, true)
        {

        }
    }
}
