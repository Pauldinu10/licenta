﻿window.sasco =
{
    EmailRegex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/gi,
    DeviceIdRegex: /^([0-9A-Fa-f]{2})-([0-9A-Fa-f]{2})-([0-9A-Fa-f]{2})-([0-9A-Fa-f]{2})-([0-9A-Fa-f]{2})-([0-9A-Fa-f]{2})+$/g,
    StartDateForCode: function (date) {

        var dateBeforeIsDst = moment(moment(date)).isDST();

        var dateAfter = moment(date).add(dateBeforeIsDst ? 13 : 14, "h");

        if (dateAfter.isDST() && !dateBeforeIsDst) {
            dateAfter.add(-2, "h");
        }

        if (!dateAfter.isDST() && dateBeforeIsDst) {
            dateAfter.add(2, "h");
        }

        return dateAfter.toDate();
    },
    EndDateForCode: function (date) {
        return window.sasco.StartDateForCode(moment(date).add(1, "day").toDate());
    }
};