﻿(function () {
    var controllerId = 'app.views.reports.schedules_company.index';
    angular.module('app').controller(controllerId, [
        '$scope', '$modal', 'abp.services.app.schedule', 'abp.services.app.user', 'abp.services.app.room',
        function($scope, $modal, scheduleService, userService, roomService) {

            $scope.users = [];

            function loadUsers() {
                userService.getList().success(function(result) {
                    for (var user in result.items)
                        $scope.users[result.items[user].id] = result.items[user].surname + " " + result.items[user].name;
                });
            };

            loadUsers();

            $scope.rooms = [];

            function loadRooms() {
                roomService.getList().success(function(result) {
                    for (var room in result.items)
                        $scope.rooms[result.items[room].id] = result.items[room].name;
                });
            };

            loadRooms();

            $scope.rows = [];
            $scope.parameterSelection = true;

            $scope.model = {
                startDate: moment().startOf("day"),
                endDate: moment().endOf("day")
            }

            $scope.loadReport = function () {
                $scope.events = scheduleService.getSchedules({
                    start: $scope.model.startDate.toISOString(),
                    end: $scope.model.endDate.toISOString(),
                    showCanceled:true
                }).success(function (result) {
                    $scope.parameterSelection = false;
                    $scope.rows = result.items;
                });
            };

            $scope.validate = function () {
                $scope.disableSave = $scope.model.startDate.isAfter($scope.model.endDate);
            }

            $scope.backToParameters = function () {
                $scope.parameterSelection = true;
            }
        }
    ]);
})();