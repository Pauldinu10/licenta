﻿(function () {
    var controllerId = 'app.views.reports.room_codes.index';

    angular.module('app').controller(controllerId, [
        '$scope', 'abp.services.app.schedule', 'abp.services.app.room', 'appSession',
        function ($scope, scheduleService, roomService, appSession) {
            $scope.rooms = [];
            roomService.getList().success(function (result) {
                for (var room in result.items)
                    $scope.rooms[result.items[room].id] = result.items[room].name;

                $scope.roomModel = {
                    list: result.items,
                    selected: result.items.length > 0 ? result.items[0] : null
                }
            });

            $scope.rows = [];
            $scope.parameterSelection = true;

            $scope.model = {
                startDate: moment().startOf("day"),
                endDate:moment().endOf("day")
            }

            $scope.loadReport = function () {
                $scope.events = roomService.getCodesForRoom({
                    start: $scope.model.startDate.toISOString(),
                    end: $scope.model.endDate.toISOString(),
                    roomId: $scope.roomModel.selected.id
                }).success(function (result) {
                    $scope.parameterSelection = false;
                    $scope.rows = result;
                });
            };

            $scope.validate = function () {
                if (appSession.user.userName !== 'boss') {
                    $scope.disableSave = moment().add(6, "month").isBefore($scope.model.endDate);
                }
                
            }

            $scope.backToParameters = function() {
                $scope.parameterSelection = true;
            };

            $scope.getTotalHours = function(schedule) {
                return moment(schedule.end).diff(moment(schedule.start), "h", true);
            };

            $scope.startDate = window.sasco.StartDateForCode;
            $scope.endDate = window.sasco.EndDateForCode;

            $scope.exportToPdf = function(reportName) {
                var model = sasco.ReportUtils.GetExportData(reportName);
                console.log(model);


                exportService.exportPDF(model).then(function (data) {
                   console.log(data);
                });
            };
        }
    ]);
})();