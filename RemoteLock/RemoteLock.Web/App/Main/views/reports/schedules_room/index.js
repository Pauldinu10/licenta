﻿(function () {
    var controllerId = 'app.views.reports.schedules_room.index';
    angular.module('app').controller(controllerId, [
        '$scope', 'abp.services.app.schedule', 'abp.services.app.user', 'abp.services.app.room',
        function ($scope, scheduleService, userService, roomService) {
            $scope.users = [];
            userService.getList().success(function(result) {
                for (var user in result.items)
                    $scope.users[result.items[user].id] = result.items[user].surname + " " + result.items[user].name;
            });

            $scope.rooms = [];
            roomService.getList().success(function (result) {
                for (var room in result.items)
                    $scope.rooms[result.items[room].id] = result.items[room].name;

                $scope.roomModel = {
                    list: result.items,
                    selected: result.items.length > 0 ? result.items[0] : null
                }
            });

            $scope.rows = [];
            $scope.parameterSelection = true;

            $scope.model = {
                startDate: moment().startOf("day"),
                endDate:moment().endOf("day")
            }

            $scope.loadReport = function () {
                $scope.events = scheduleService.getSchedules({
                    start: $scope.model.startDate.toISOString(),
                    end: $scope.model.endDate.toISOString(),
                    roomId: $scope.roomModel.selected.id,
                    showCanceled: true
                }).success(function (result) {
                    $scope.parameterSelection = false;
                    $scope.rows = result.items;
                });
            };

            $scope.backToParameters = function() {
                $scope.parameterSelection = true;
            };

            $scope.getTotalHours = function(schedule) {
                return moment(schedule.end).diff(moment(schedule.start), "h", true);
            };

            $scope.exportToPdf = function(reportName) {
                var model = sasco.ReportUtils.GetExportData(reportName);
                console.log(model);


                exportService.exportPDF(model).then(function (data) {
                    console.log(data);
                });
            };
        }
    ]);
})();