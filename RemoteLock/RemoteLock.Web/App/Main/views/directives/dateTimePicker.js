﻿(function () {
    angular.module('app').directive('dateTimePicker', function () {
        return {
            require: '?ngModel',
            restrict: 'AE',
            scope: {
                useCurrent: '@',
                format: '@',
                language: '@',
                change: '&onChange',
                ngModel: '='
            },
            link:function(scope, elem, attrs, ngModel){
                var jq = elem.datetimepicker({
                    useCurrent: scope.useCurrent,
                    widgetPositioning: { horizontal: "right" },
                    format: scope.format,
                    locale: moment.locale(),
                    defaultDate: moment(ngModel)
                });

                var dpChange = function(e) {
                    ngModel.$setViewValue(e.date);
                    scope.$apply();
                    scope.change();
                };

                elem.on("dp.change", dpChange);

                scope.$watch('ngModel', function (date) {
                    if (date && jq) {
                        elem.off("dp.change", dpChange);
                        jq.data("DateTimePicker").date(date);
                        if (scope.showBlank && isNaN(date.valueOf())) {
                            jq.data("DateTimePicker").defaultDate(null);
                            jq.data("DateTimePicker").date(null);
                        }
                        elem.on("dp.change", dpChange);
                    }
                });

                scope.$watch('language', function (language) {
                    if (language && jq) {
                        jq.data("DateTimePicker").format(scope.format);
                        jq.data("DateTimePicker").locale(moment());
                    }
                });
            }
        }
    });
})();