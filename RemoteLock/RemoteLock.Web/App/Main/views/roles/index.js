﻿(function() {
    angular.module('app').controller('app.views.roles.index', [
        '$scope', '$modal', 'abp.services.app.role',
        function ($scope, $modal, roleServices) {
            var vm = this;

            vm.roles = [];

            vm.permissions = [];

            for (var perm in abp.auth.allPermissions) {
                if (abp.auth.allPermissions.hasOwnProperty(perm)) {
                    vm.permissions.push(perm);
                }
            }
            vm.permissions.sort();

            vm.selectedPerm = vm.permissions[0];
            function getRoles() {
                roleServices.getRoles({}).success(function (result) {
                    vm.roles = result.items;
                    if (!vm.selectedRole)
                        vm.selectedRole = result.items[0];
                });
            }

            vm.grant = function() {
                roleServices.updateRolePermissions({
                    roleId: vm.selectedRole.roleId,
                    permission: vm.selectedPerm,
                    grants: true
                }).success(function(result) {
                    getRoles();
                });
            };

            vm.prohibit = function () {
                roleServices.updateRolePermissions({
                    roleId: vm.selectedRole.roleId,
                    permission: vm.selectedPerm,
                    grants: false
                }).success(function (result) {
                    getRoles();
                });
            }

            getRoles();
        }
    ]);
})();