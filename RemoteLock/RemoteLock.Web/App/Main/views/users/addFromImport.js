﻿(function () {
    angular.module('app').controller('app.views.users.addFromImport', [
        '$scope', '$modalInstance', 'abp.services.app.locations', 'abp.services.app.vehicleCategories', 'abp.services.app.user', 'appSession',
        function ($scope, $modalInstance, locationsService, vehicleCategoriesService, userService, appSession) {

            var usersEmailList = [];
            var usernames = [];

            $scope.isValid = true;
            $scope.validate = function (user) {
                var validate = (user.nume != null && user.nume.length >= 2);
                validate = validate && (user.prenume != null && user.prenume.length >= 2);
                validate = validate && (user.parola != null && user.parola.length >= 2);

                user.isEmailValid = !!(user.email &&
                    user.email.match(window.sasco.EmailRegex) &&
                    usersEmailList.indexOf(user.email) === -1);

                user.isUserNameValid = usernames.indexOf(user.utilizator) === -1;

                user.isValid = validate && user.isEmailValid && user.isUserNameValid;

                if (user.isValid) {
                    usersEmailList.push(user.email);
                    usernames.push(user.utilizator);
                } else
                    $scope.isValid = false;
            }

            userService.getList().success(function(result) {
                for (var index = 0; index < result.items.length; index++) {
                    usersEmailList.push(result.items[index].emailAddress);
                    usernames.push(result.items[index].username);
                }

                for (var i = 0; i < $scope.csv.result.length; i++) {
                    $scope.validate($scope.csv.result[i]);
                }
            });

            $scope.isORM = abp.auth.hasAllOfPermissions("Management", "ORM.Reports");

            $scope.cancel = function () { 
                $modalInstance.dismiss();
            };

            $scope.save = function () {
                for (var i = 0; i < $scope.csv.result.length; i++)
                    $scope.saveIndividual($scope.csv.result[i]);

                $scope.isValid = false;
                $scope.count = $scope.csv.result.length;

                $scope.$watch("count", function(o, n) {
                    if ($scope.count === 0)
                        $modalInstance.close();
                });
            };

            $scope.saveIndividual = function(user) {
                userService.add({
                    name: user.nume,
                    surname: user.prenume,
                    locationId: null,
                    username: user.utilizator,
                    password:user.parola,
                    emailAddress: user.email,
                    vehicleCategoryId: null,
                    isManager: false,
                    userType: null,
                    isORM: $scope.isORM
                }).success(function () {
                    user.isUserCreated = true;
                    $scope.count--;
                });
            };
        }
    ]);
})();