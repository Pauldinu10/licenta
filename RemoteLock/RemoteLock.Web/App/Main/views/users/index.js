﻿(function () {
    var controllerId = 'app.views.users.index';
    angular.module('app').controller(controllerId, [
        '$scope', '$uibModal', 'abp.services.app.user','appSession',
        function ($scope, $uibModal, userService, appSession) {
            var vm = this;

            vm.users = [];

            function loadUsers() {
                userService.getList().success(function (result) {
                    vm.users = result.items;

                });
            };

            vm.isORM = abp.auth.hasAllOfPermissions("Management", "ORM.Reports");

            vm.toggleSuspendedState = function (user) {
                userService.toggleUserSuspended(user.id).success(function (result) {
                    user.isActive = !user.isActive;
                });
            }

            vm.openNewUserDialog = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/users/add.cshtml',
                    controller: 'app.views.users.add',
                    size: 'md'
                });

                modalInstance.result.then(function (model) {
                    userService.add(model).success(function (result) {
                        loadUsers();
                    });
                });

                modalInstance.result.catch(function () {
                    //handle error
                });
            }

            loadUsers();

            vm.currentUser = appSession.user.emailAddress;

            function initCSV()
            {
                console.log("init csv");
                vm.csv = {
                    content: null,
                    header: !0,
                    headerVisible: false,
                    separator: ",",
                    separatorVisible: false,
                    result: null,
                    accept: '.csv',
                    encoding: "ISO-8859-1",
                    encodingVisible: false,
                    callback: function () {
                        $scope.csv = vm.csv;
                        var modalInstance = $uibModal.open({
                            templateUrl: '/App/Main/views/users/addFromImport.cshtml',
                            controller: 'app.views.users.addFromImport',
                            size: 'lg',
                            scope: $scope
                        });

                        modalInstance.result.then(function (model) {
                            loadUsers();

                            initCSV();
                        });

                        modalInstance.result.catch(function () {
                            initCSV();
                        });
                    }
                };
            }

            initCSV();

            vm.edit = function (model) {

                $scope.model = {
                    id: model.id,
                    name: model.name,
                    surname: model.surname,
                    username:model.username,
                    emailAddress:model.emailAddress,
                    isManager: model.isManager,
                    userType:model.userType,
                    isORM:model.isORM
                };

                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/users/add.cshtml',
                    controller: 'app.views.users.add',
                    size: 'md',
                    scope: $scope
                });

                modalInstance.result.then(function (model) {
                    userService.update(model).success(function (result) {
                        loadUsers();
                    });
                });

                modalInstance.result.catch(function () {
                    //handle error
                });
            };
        }
    ]);
})();