﻿(function () {
    angular.module('app').controller('app.views.users.add', [
        '$scope', '$modalInstance', 'abp.services.app.user', 'appSession',
        function($scope, $modalInstance, userService, appSession) {

            var usersEmailList = [];
            var usernames = [];

            userService.getList().success(function(result) {
                for (var index = 0; index < result.items.length; index++) {
                    usersEmailList.push(result.items[index].emailAddress);
                    usernames.push(result.items[index].username);
                }
            });

            $scope.util = {

            };

            $scope.ctrl =
            {
                validate: true,
                showRedUsername: false,
                showGreenUsername: false,
                showRed: false,
                showGreen: false,
                hideEmailAndPass: $scope.model != null,
                showResetPassword: true,
                showORMCheckBox: abp.auth.hasAllOfPermissions("Management", "ORM.Reports","Reports")
            };

            if (!$scope.model)
                $scope.model = {
                };
            else $scope.ctrl.hideManagement = appSession.user.emailAddress == $scope.model.emailAddress;

            $scope.model.isORM = abp.auth.hasAllOfPermissions("Management", "ORM.Reports");

            $scope.validate = function () {
                var validate = ($scope.model.name != null && $scope.model.name.length >= 2);
                validate = validate && ($scope.model.surname != null && $scope.model.surname.length >= 2);

                if (!$scope.ctrl.hideEmailAndPass) {
                    validate = $scope.ctrl.confirmPassword == $scope.model.password;
                    validate = validate && ($scope.model.password != null && $scope.model.password.length >= 2);

                    if ($scope.model && $scope.model.emailAddress && $scope.model.emailAddress.match(window.sasco.EmailRegex) && usersEmailList.indexOf($scope.model.emailAddress) == -1) {
                        $scope.ctrl.showGreen = true;
                        $scope.ctrl.showRed = false;
                    } else {
                        $scope.ctrl.showGreen = false;
                        validate = false;
                        $scope.ctrl.showRed = $scope.model.emailAddress != null && $scope.model.emailAddress.length > 0;
                    }

                    if ($scope.model.username) {
                        if (usernames.indexOf($scope.model.username) == -1) {
                            $scope.ctrl.showGreenUsername = true;
                            $scope.ctrl.showRedUsername = false;
                        } else {
                            $scope.ctrl.showGreenUsername = false;
                            validate = false;
                            $scope.ctrl.showRedUsername = true;
                        }
                    }
                }

                $scope.ctrl.validate = validate;
            }

            $scope.validate();

            $scope.cancel = function () { 
                $modalInstance.dismiss();
            };

            $scope.save = function () {
                $modalInstance.close($scope.model);
            };

            $scope.reset=function() {
                userService.resetPassword($scope.model.id)
                    .success(function() {
                        $scope.ctrl.showResetPassword = false;
                    });
            }
        }
    ]);
})();