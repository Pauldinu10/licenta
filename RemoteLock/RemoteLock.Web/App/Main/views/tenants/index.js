﻿(function() {
    angular.module('app').controller('app.views.tenants.index', [
        '$scope', '$modal', 'abp.services.app.tenant',
        function ($scope, $modal, tenantService) {
            var vm = this;

            vm.tenants = [];

            function getTenants() {
                tenantService.getTenants({}).success(function (result) {
                    vm.tenants = result.items;
                });
            }

            getTenants();
        }
    ]);
})();