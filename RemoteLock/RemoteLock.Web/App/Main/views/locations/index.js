﻿(function () {
    var controllerId = 'app.views.locations.index';
    angular.module('app').controller(controllerId, [
        '$scope', '$uibModal', 'abp.services.app.locations',
        function ($scope, $uibModal, locationsService) {
            var vm = this;

            vm.locations = [];

            function loadLocations() {
                locationsService.getAll().success(function (result) {
                    vm.locations = result.items;
                });
            };

            vm.OpenAddNewLocationDialog = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/locations/add.cshtml',
                    controller: 'app.views.locations.add',
                    size: 'md'
                });

                modalInstance.result.then(function (model) {
                    locationsService.create(model).success(function (result) {
                        loadLocations();
                    });
                });


                modalInstance.result.catch(function () {
                    //handle error
                });
            };

            vm.edit = function (model) {

                $scope.model = {
                    name: model.name,
                    id: model.id,
                    description: model.description
                };

                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/locations/add.cshtml',
                    controller: 'app.views.locations.add',
                    size: 'md',
                    scope:$scope
                });

                modalInstance.result.then(function (model) {
                    locationsService.update(model).success(function (result) {
                        loadLocations();
                    });
                });

                modalInstance.result.catch(function () {
                    //handle error
                });
            };

            vm.delete = function (model) {
                locationsService.delete(model.id).success(function(result) {
                    loadLocations();
                });
            };

            loadLocations();
        }
    ]);
})();