﻿(function () {
    angular.module('app').controller('app.views.locations.add', [
        '$scope', '$modalInstance',
        function ($scope, $modalInstance) {

            $scope.ctrl = {
                validate: false
            };

            if (!$scope.model) {
                $scope.model = {
                };
            }
            else
                $scope.ctrl.IsEdit = true;

            $scope.cancel = function () {
                $modalInstance.dismiss();
            };

            $scope.save = function () {
                $modalInstance.close($scope.model);
            };


            $scope.validate = function () {
                var validate = ($scope.model.name != null && $scope.model.name.length >= 2);
                validate = validate && ($scope.model.address != null && $scope.model.address.length >= 2);

                $scope.ctrl.validate = validate;
            }

            $scope.validate();
        }
    ]);
})();