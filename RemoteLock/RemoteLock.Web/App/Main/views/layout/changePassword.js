﻿(function () {
    angular.module('app').controller('app.views.layout.changePassword', [
        '$scope', '$modalInstance', 'abp.services.app.user',
        function ($scope, $modalInstance, userService) {

            if (!$scope.model)
                $scope.model = {

                };

            $scope.ctrl =
            {
                validate: false,
                changed: false,
                confirmPassword: null,
                showError: false
            };

            $scope.validate = function () {
                $scope.ctrl.showError = false;
                var validate = ($scope.model.newPassword != null && $scope.model.newPassword.length >= 2);
                validate = validate && $scope.model.newPassword == $scope.ctrl.confirmPassword;

                validate = validate && ($scope.model.oldPassword != null && $scope.model.oldPassword.length >= 2);

                $scope.ctrl.validate = validate;
            }

            $scope.close = function () {
                $modalInstance.dismiss();
            };

            $scope.change = function () {
                userService.changePassword($scope.model)
                    .success(function(result) {
                        $scope.ctrl.changed = result;
                        $scope.ctrl.showError = !result;
                        $scope.ctrl.validate = result;
                    });
            };
        }
    ]);
})();