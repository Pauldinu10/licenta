﻿(function () {
    var controllerId = 'app.views.layout.header';
    angular.module('app').controller(controllerId, [
        '$rootScope', '$state', 'appSession', '$uibModal',
        function ($rootScope, $state, appSession, $uibModal) {
            var vm = this;

            vm.languages = abp.localization.languages;
            vm.currentLanguage = abp.localization.currentLanguage;

            vm.menu = abp.nav.menus.MainMenu;
            vm.currentMenuName = $state.current.menu;

            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                vm.currentMenuName = toState.menu;
            });

            vm.getShownUserName = function () {
                if (!abp.multiTenancy.isEnabled) {
                    return appSession.user.userName;
                } else {
                    if (appSession.tenant) {
                        return appSession.tenant.tenancyName + '\\' + appSession.user.userName;
                    } else {
                        return '.\\' + appSession.user.userName;
                    }
                }
            };

            abp.event.on('abp.notifications.received', function (userNotification) {
                console.log(userNotification);
            });

            vm.ShowChangePasswordDialog=function() {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/layout/changePassword.cshtml',
                    controller: 'app.views.layout.changePassword',
                    size: 'md'
                });
            }

            vm.ClosePanel = function () {
                var toggleBtn = angular.element('#closePanel');
                if (toggleBtn.css("display")!=="none")
                    toggleBtn.trigger('click');
            };
        }
    ]);
})();