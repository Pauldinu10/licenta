﻿(function () {
    angular.module('app').controller('app.views.schedule.error', [
        '$scope', '$modalInstance',
        function ($scope, $modalInstance) {
            $scope.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();