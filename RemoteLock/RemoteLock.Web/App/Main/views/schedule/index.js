﻿(function () {
    var controllerId = 'app.views.schedule.index';
    angular.module('app').controller(controllerId, [
        '$q', '$scope', '$uibModal', 'abp.services.app.room', "$timeout", 'abp.services.app.schedule','appSession',
        function ($q, $scope, $uibmodal, roomService, $timeout, scheduleService, appSession) {

            $scope.events = [];

            $scope.DisabledUI = appSession.user.userName === 'sas';

            $scope.loadEvents = function () {
                $timeout(function() {
                    roomService.getList().success(function(result) {
                        var items = result.items;

                        var resources = [];

                        items.forEach(function(room) {
                            resources.push({
                                "id": room.id,
                                "name": room.name
                            });
                        });
                        $timeout(function() {
                            $scope.config.resources = resources;
                        });
                    });
                });

                scheduleService.getSchedules({
                    start: moment().startOf("year").toISOString(),
                    end: moment().add(1,"y").endOf("year").toISOString()
                }).success(function(result) {
                    var events = result.items;
                    $scope.events = events;
                });
            }

            $scope.loadEvents();

            var bubble = new DayPilot.Bubble();
            bubble.animated = false;
            bubble.backgroundColor = '#ffffff';
            bubble.cssOnly = true;
            bubble.theme = 'bubble_default';
            bubble.hideAfter = 500;
            bubble.loadingText = 'Loading...';
            bubble.position = 'Mouse';
            bubble.showAfter = 100;
            bubble.showLoadingLabel = true;
            bubble.useShadow = true;
            bubble.zIndex = 10;
            bubble.border = '1px solid #000000';
            bubble.width = '200px';

            var getCurrentScope = function (id) {
                return angular.element(document.getElementById(id)).scope();
            };
            var isSmallScreen=function() {
                return $(window).width() < 768;
            }

            $scope.navigatorConfig = {
                selectMode: isSmallScreen()? "day" : "week",
                showMonths: 3,
                skipMonths: 1,
                weekStarts: 1,
                theme: "navigator_blue",
                onTimeRangeSelected: function (args) {
                    $scope.weekConfig.startDate = args.day;
                    $scope.dayConfig.startDate = args.day;
                    //$scope.loadEvents();
                }
            };



            var clearRangeSelection = function () {
                //todo
            };

            var handleRangeSelection = function (args) {
                console.log(args);

                if (!moment(args.start.value).isBefore(moment().startOf("day"))){
                    $scope.descriptionArgs = angular.copy(args);

                    var modalInstance2 = $uibmodal.open({
                        templateUrl: '/App/Main/views/schedule/description.cshtml',
                        controller: 'app.views.schedule.description',
                        backdrop: 'static',
                        scope: $scope
                    });

                    modalInstance2.result.then(function (data) {
                        clearRangeSelection();
                        scheduleService.create({
                                start: data.startDate,
                                end: data.endDate,
                                roomId:args.resource,
                                name: data.name,
                                phone:data.phone,
                                email:data.email,
                                notes:data.notes
                            })
                            .success(function(result) {
                                $scope.loadEvents();
                            });
                    });

                    modalInstance2.result.catch(function() {
                        clearRangeSelection();
                    });
                } else {
                    clearRangeSelection();
                    var modalInstance = $uibmodal.open({
                        templateUrl: '/App/Main/views/schedule/error.cshtml',
                        controller: 'app.views.schedule.error',
                        backdrop: 'static'
                    });
                }
            };

            var calendarHeight = window.innerHeight - (isSmallScreen() ? 140 : 300);

            $scope.showDay = function () {
                $scope.dayConfig.visible = true;
                $scope.weekConfig.visible = false;
                $scope.navigatorConfig.selectMode = "day";
            };

            $scope.showWeek = function () {
                $scope.dayConfig.visible = false;
                $scope.weekConfig.visible = true;
                $scope.navigatorConfig.selectMode = "week";
            };

            var currentDate = null;

            $scope.selectNext = function () {
                $timeout(function () {
                    var schedulerScope = getCurrentScope("dp");

                    schedulerScope.dp.locale = 'ro-RO';
                    currentDate=currentDate.addDays($scope.config.scale === 'day' ? 1 : 7);

                    schedulerScope.dp.scrollTo(currentDate);
                });
            };

            $scope.selectPrev = function () {
                $timeout(function () {
                    var schedulerScope = getCurrentScope("dp");
                    currentDate= currentDate.addDays($scope.config.scale === 'day' ? -1 : -7);

                    schedulerScope.dp.scrollTo(currentDate);
                });
            };

            abp.notifications.messageFormatters['RemoteLock.Schedule.Notifications.SendScheduledNotificationData'] = function (userNotification) {
                return abp.localization.localize("scheduled_notification_message", "RemoteLock");
            };

            var eventCallback= function(userNotification) {

                if (userNotification.notification.data.locationId === $scope.locations.selected.id &&
                    userNotification.notification.data.userId !== appSession.user.id) {
                    $scope.loadEvents();
                    abp.notifications.showUiNotifyForUserNotification(userNotification);
                }
            }

            abp.event.on('abp.notifications.received', eventCallback);

            $scope.$on('$destroy', function (){
                abp.event.off('abp.notifications.received', eventCallback);
            });

            $scope.config = {
                scale: "Day",
                days: 700,
                bubble: new DayPilot.Bubble(),
                resourceBubble: new DayPilot.Bubble(),
                startDate: moment().startOf("year").toISOString(),
                locale: abp.localization.currentLanguage.name,
                height: calendarHeight,
                heightSpec :'Max',
                eventClickHandling: "Disabled",
                eventMoveHandling: "Disabled",
                eventResizeHandling: "Disabled",
                onBeforeCellRender : function (args) {
                    if (args.cell.start <= DayPilot.Date.today() && DayPilot.Date.today() < args.cell.end) {
                        args.cell.backColor = "#ffcccc";

                    }

                    if (!currentDate) {
                        currentDate = DayPilot.Date.today();
                        $scope.selectPrev();
                        console.log("Scrolled");
                    }
                },
                onEventMoved: function (args) {
                    console.log(args.e);
                },
                onEventSelected: function (args) {
                    $scope.$apply(function () {
                        $scope.selectedEvents = $scope.dp.multiselect.events();
                    });
                },
                onBeforeEventRender: function (args) {
                    var prefix = "";

                    if (args.data.name)
                        prefix += "<div><b>" + args.data.name + "</b></div>";

                    if (args.data.phone)
                        prefix += "<div>" + args.data.phone + "</div>";
                    
                    if (args.data.email) 
                        prefix += "<div>" + args.data.email + "</div>";

                    if (args.data.notes)
                        prefix += "<div><b>" + args.data.notes + "</b></div>";

                    args.data.bubbleHtml = prefix
                        + "<div>" + abp.localization.localize("schedule_start_date", "RemoteLock") + ": " + new DayPilot.Date(args.data.start).toString(abp.localization.localize("DateFormatAngular", "RemoteLock")) + "</div>"
                        + "<div>" + abp.localization.localize("schedule_end_date", "RemoteLock") + ": " + new DayPilot.Date(args.data.end).toString(abp.localization.localize("DateFormatAngular", "RemoteLock")) + "</div>";
                },
                onTimeRangeSelected : handleRangeSelection,
                contextMenu: new DayPilot.Menu({
                    items: [
                        {
                            text: abp.localization.localize("schedule_show_access_codes", "RemoteLock"), onclick: function () {
                                roomService.getCodeForSchedule(this.source.data.id)
                                    .success(function (data) {
                                        $scope.codes = data;
                                        $scope.startDate = window.sasco.StartDateForCode;
                                        $scope.endDate = window.sasco.EndDateForCode;
                                        var modalInstance = $uibmodal.open({
                                            templateUrl: '/App/Main/views/schedule/codeDisplay.cshtml',
                                            controller: 'app.views.schedule.error',
                                            backdrop: 'static',
                                            scope: $scope
                                        });
                                    });
                            }
                        },
                        {
                            text: abp.localization.localize("codes_email", "RemoteLock"), onclick: function () {
                                scheduleService.sendCodesToEmail(this.source.data.id)
                                    .success(function (data) {
                                        abp.notify.success(abp.localization.localize("codes_email_confirm", "RemoteLock") + data);
                                    });
                            }
                        },
                        {
                            text: abp.localization.localize("Change", "RemoteLock"), onclick: function () {
                                var schedule = this.source.data;

                                $scope.model = {
                                    roomId: schedule.resource,
                                    start: schedule.start,
                                    end: schedule.end,
                                    scheduleId: schedule.id
                                }

                                var modalInstance = $uibmodal.open({
                                    templateUrl: '/App/Main/views/report/change_schedule.cshtml',
                                    controller: 'app.views.report.changeSchedule',
                                    size: 'md',
                                    scope: $scope
                                });

                                modalInstance.result.then(function (reason) {
                                    $scope.loadEvents();
                                });

                                modalInstance.result.catch(function () {
                                    //handle error
                                });
                            }
                        },
                        {
                            text: abp.localization.localize("Cancel", "RemoteLock"), onclick: function () {

                                var modalInstance = $uibmodal.open({
                                    templateUrl: '/App/Main/views/report/cancel_schedule.cshtml',
                                    controller: 'app.views.report.cancelSchedule',
                                    size: 'md'
                                });

                                var schedule = this.source.data;

                                modalInstance.result.then(function (reason) {
                                    var promise = scheduleService.cancelSchedule({
                                        scheduldId: schedule.id ,
                                        cancelReason: reason,
                                        reassingDifferent: false
                                    });

                                    promise.success(function (data) {
                                        schedule.canceled = true;
                                        $scope.loadEvents();
                                    });
                                });

                                modalInstance.result.catch(function () {
                                    //handle error
                                });
                            }
                        }  //todo serverside
                    ]
                }),
                timeHeaders: [
                    { groupBy: "Month" },
                    { groupBy: "Cell", format: "d" }
                ]
            };

            $scope.scrollTo = function (date) {
                $scope.dp.scrollTo(moment(date));
            };

            $scope.scale = function (val) {
                $scope.config.scale = val;
                $timeout(function () {
                    var schedulerScope = getCurrentScope("dp");
                    schedulerScope.dp.scrollTo(currentDate);
                });
            };
        }
    ]);
})();