﻿(function () {
    angular.module('app').controller('app.views.schedule.description', [
        '$scope', '$modalInstance',
        function ($scope, $modalInstance) {

            $scope.disableSave = true;

            $scope.description = {
                startDate: moment($scope.descriptionArgs.start.value).toDate(),
                endDate: moment($scope.descriptionArgs.end.value).toDate()
            };

            $scope.cancel = function () {
                $modalInstance.dismiss();
            };

            $scope.validate = function() {
                $scope.disableSave = $scope.description.startDate > $scope.description.endDate ||
                    moment($scope.description.startDate).isBefore(moment().startOf("day")) ||
                    !$scope.description.name ||
                    (!$scope.description.email && !$scope.description.phone) ||
                    ($scope.description.email && !$scope.description.email.match(window.sasco.EmailRegex));
            };

            $scope.save = function () {

                $scope.description.startDate = $scope.description.startDate.toISOString();
                $scope.description.endDate = $scope.description.endDate.toISOString();

                $modalInstance.close($scope.description);
            };
        }
    ]);
})();