﻿(function () {
    angular.module('app').filter('rlRoomFilter', function() {
        return function (items,filter) {
            var filtered = [];

            var lcFilter = filter ? filter.toLowerCase() : "";

            for (var i = 0; i < items.length; i++) {
                var room = items[i];
                if (lcFilter.length === 0 ||
                    room.name.toLowerCase().indexOf(lcFilter) >= 0 ||
                    room.description.toLowerCase().indexOf(lcFilter) >= 0 ||
                    room.deviceId.toLowerCase().indexOf(lcFilter) >= 0
                )
                    filtered.push(room);
            }
            return filtered;
          };
      });
})();