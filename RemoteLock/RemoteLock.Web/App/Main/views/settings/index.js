﻿(function () {
    var controllerId = 'app.views.settings.index';
    angular.module('app').controller(controllerId, [
        '$scope','abp.services.app.settings',
        function ($scope, settingsService) {
            var vm = this;

            vm.showSettings = abp.auth.hasPermission('Settings');

            function loadSettings() {
                settingsService.getSettings().success(function (result) {
                    vm.model = result;
                });
            };

            vm.save = function () {
                settingsService.updateSettings(vm.model).success(function (result) {
                    abp.notify.success(abp.localization.localize("users_file_upload_modal_saved", "RemoteLock"));
                    loadSettings();
                });
            };
            loadSettings();
        }
    ]);
})();