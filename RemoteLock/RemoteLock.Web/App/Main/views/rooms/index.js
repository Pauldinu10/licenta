﻿(function () {
    var controllerId = 'app.views.rooms.index';
    angular.module('app').controller(controllerId, [
        '$scope', '$uibModal', 'abp.services.app.room',
        function ($scope, $uibModal, roomService) {
            var vm = this;

            vm.rooms = [];
            function loadRooms() {
                roomService.getList().success(function (result) {
                    vm.rooms = result.items;
                });
            };

            vm.openNewRoomDialog = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/rooms/add.cshtml',
                    controller: 'app.views.rooms.add',
                    size: 'md'
                });

                modalInstance.result.then(function (model) {
                    roomService.create(model).success(function (result) {
                        loadRooms();
                    });
                });

                modalInstance.result.catch(function () {
                    //handle error
                });
            }

            vm.toggleDisabledState= function(vehicle) {
                roomService.toggleVehicleDisabledState(vehicle.id).success(function (result) {
                    vehicle.disabled = !vehicle.disabled;
                });
            }

            loadRooms();

            vm.edit = function (model) {

                $scope.model = {
                    id: model.id,
                    name: model.name,
                    description: model.description,
                    deviceId: model.deviceId,
                    address: model.address
                };

                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/rooms/add.cshtml',
                    controller: 'app.views.rooms.add',
                    size: 'md',
                    scope: $scope
                });

                modalInstance.result.then(function (model) {
                    roomService.update(model).success(function (result) {
                        loadRooms();
                    });
                });

                modalInstance.result.catch(function () {
                    //handle error
                });
            };

            vm.getCodes = function (model) {
                roomService.getTodayCodes(model.id).success(function (result) {

                    $scope.cod = result;

                    var modalInstance = $uibModal.open({
                        templateUrl: '/App/Main/views/rooms/showCodes.cshtml',
                        controller: 'app.views.rooms.showCodes',
                        size: 'md',
                        scope: $scope
                    });
                });
            }

            vm.delete = function (model) {
                roomService.delete(model.id).success(function (result) {
                    loadRooms();
                    abp.notify.success(model.name+abp.localization.localize("rooms_model_deleted_sufix", "RemoteLock"));
                });
            };
        }
    ]);
})();