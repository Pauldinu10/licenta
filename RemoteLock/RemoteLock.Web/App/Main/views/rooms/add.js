﻿(function () {
    angular.module('app').controller('app.views.rooms.add', [
        '$scope', '$modalInstance',
        function ($scope, $modalInstance, locationsService) {

            if (!$scope.model)
                $scope.model = {

                };
            else {
                $scope.IsEdit = true;
            }

            $scope.cancel = function () { 
                $modalInstance.dismiss();
            };

            $scope.disableSave = true;

            $scope.validate = function () {
                var isValid = $scope.model.deviceId &&
                    $scope.model.deviceId.match(window.sasco.DeviceIdRegex) &&
                    $scope.model.name.length > 0 &&
                    $scope.model.description.length > 0 &&
                    $scope.model.address.length > 0;

                $scope.disableSave = !isValid;
            }

            $scope.save = function () {
                $modalInstance.close($scope.model);
            };
        }
    ]);
})();