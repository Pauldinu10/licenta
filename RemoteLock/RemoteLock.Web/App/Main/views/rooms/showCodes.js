﻿(function () {
    angular.module('app').controller('app.views.rooms.showCodes', [
        '$scope', '$modalInstance',
        function ($scope, $modalInstance) {

            $scope.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();