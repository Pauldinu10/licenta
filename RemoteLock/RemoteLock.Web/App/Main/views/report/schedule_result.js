﻿(function () {
    var controllerId = 'app.views.report.schedule_result';
    angular.module('app').controller(controllerId, [
        '$scope', '$uibModal', 'abp.services.app.schedule', 'abp.services.app.user', 'abp.services.app.room',
        function ($scope, $uibModal, scheduleService, userService, roomService) {

            $scope.getTotalMinutes = sasco.ReportUtils.GetTotalMinutes;
            $scope.exportToExcel = sasco.ReportUtils.ExportExcel;

            $scope.showCancelButton = function(schedule) {
                return !schedule.canceled && moment(schedule.end).isAfter(moment());
            };

            $scope.showFirstFill = function (schedule) {
                return !$scope.showCancelButton(schedule) && !schedule.canceled;
            };

            $scope.cancelSchedule = function (schedule) {

                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/report/cancel_schedule.cshtml',
                    controller: 'app.views.report.cancelSchedule',
                    size: 'md'
                });

                modalInstance.result.then(function (reason) {
                    var promise = scheduleService.cancelSchedule({
                        scheduldId: schedule.id,
                        cancelReason:reason,
                        reassingDifferent: false
                    });

                    promise.success(function (data) {
                        schedule.canceled = true;
                        $scope.loadReport();
                    });
                });

                modalInstance.result.catch(function () {
                    //handle error
                });
            };

            $scope.sendCodesOnEmail = function(schedule) {
                scheduleService.sendCodesToEmail(schedule.id)
                    .success(function (data) {
                        abp.notify.success(abp.localization.localize("codes_email_confirm", "RemoteLock") + data);
                    });
            }

            $scope.changeSchedule = function (schedule) {
                $scope.model= {
                    roomId: schedule.resource,
                    start: schedule.start,
                    end: schedule.end,
                    scheduleId:schedule.id
                }

                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/report/change_schedule.cshtml',
                    controller: 'app.views.report.changeSchedule',
                    size: 'md',
                    scope: $scope
                });

                modalInstance.result.then(function (reason) {
                    $scope.loadReport();
                });

                modalInstance.result.catch(function () {
                    //handle error
                });
            }
        }
    ]);
})();
