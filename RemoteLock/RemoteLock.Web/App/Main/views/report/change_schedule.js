﻿(function () {
    angular.module('app').controller('app.views.report.changeSchedule', [
        '$scope', '$modalInstance', 'abp.services.app.room','abp.services.app.schedule',
        function ($scope, $modalInstance, roomService, scheduleService) {

            function loadRooms() {
                roomService.getList().success(function (result) {
                    for (var room in result.items)
                        $scope.rooms[result.items[room].id] = result.items[room].name;
                });
            };

            var currentRoom = null;
            if (!$scope.rooms) {
                $scope.rooms = [];

                loadRooms();
            }


            $scope.GetAvailableRooms = function () {
                scheduleService.getAvailableRoomsForScheduleChange({
                    start: $scope.model.start,
                    end: $scope.model.end
                }).success(function (result) {

                    result.items.push($scope.model.resource);

                    var currentRoomId = result.items.indexOf($scope.model.roomId);

                    $scope.roomIds = {
                        list: result.items,
                        selected: result.items[Math.max(currentRoomId,0)]
                    }

                    $scope.validate();
                });
            };
            if ($scope.model.roomId) {
                roomService.get($scope.model.roomId).success(function(room) {
                    currentRoom = room;
                    $scope.GetAvailableRooms();
                });

            } else {
                $scope.GetAvailableRooms();
            }

            $scope.disableSave = true;

            $scope.cancel = function () {
                $modalInstance.dismiss();
            };

            $scope.save = function () {
                scheduleService.changeSchedule({
                    scheduleId: $scope.model.scheduleId,
                    newRoomId: $scope.roomIds.selected
                }).success(function(result) {
                    $modalInstance.close($scope);
                });
            };

            $scope.validate = function () {
                $scope.disableSave = $scope.roomIds == null || $scope.roomIds.selected == null || $scope.roomIds.selected === currentRoom.id;
            };
        }
    ]);
})();