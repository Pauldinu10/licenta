﻿window.sasco.ReportUtils = {
    ExportExcel: function(actionPost, actionDownload, reportName) {
        var model = sasco.ReportUtils.GetExportData(reportName);
        $.post(actionPost,
            model,
            function(data, status) {
                var url = actionDownload + '?id=' + data + '&reportName=' + reportName + '.xls';
                window.location = url;
            });
    },
    GetExportData: function(reportname, startColIndex, endColIndex) {
        var result = {
            name: reportname.replace("/", "_"),
            headerStrings: [],
            data: []
        };

        var table = $("#toExportTable");

        var headers = table.find('th');

        if (!startColIndex)
            startColIndex = 0;

        if (!endColIndex)
            endColIndex = headers.length - 1;

        for (var index = startColIndex; index <= endColIndex; index++)
            result.headerStrings.push(headers.eq(index).text().toString());

        var rows = table.find('tbody > tr');
        var currentRow = 0;
        for (var rowIndex = 0; rowIndex < rows.length - 1; rowIndex++) {

            var cells = rows.eq(rowIndex).find('td');

            var internalRows = 1;
            for (var cellIndex = startColIndex; cellIndex <= endColIndex; cellIndex++) {
                if (cells.eq(cellIndex).find('li').length > 1)
                    internalRows = cells.eq(cellIndex).find('li').length;
            }
            for (var liIndex = 0; liIndex < internalRows; liIndex++) {
                var rowData = [];
                for (var cellIndex = startColIndex; cellIndex <= endColIndex; cellIndex++) {
                    if (cells.eq(cellIndex).find('li').length > 1)
                        rowData.push(cells.eq(cellIndex).find('li').eq(liIndex).text().trim());
                    else
                        rowData.push(cells.eq(cellIndex).text().trim());
                }
                result.data[currentRow] = rowData;
                currentRow++;
            }
        }

        return result;
    },
    GetTotalHours: function(schedule) {
        return moment(schedule.end).diff(moment(schedule.start), "h", true);
    },

    GetTotalMinutes: function(schedule) {
        return moment(schedule.end).diff(moment(schedule.start), "m", true);
    }
}