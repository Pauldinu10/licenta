﻿(function () {
    angular.module('app').controller('app.views.report.cancelSchedule', [
        '$scope', '$modalInstance',
        function ($scope, $modalInstance) {
            $scope.disableSave = true;

            $scope.model = {
                cancelReason: ""
            };

            $scope.cancel = function () {
                $modalInstance.dismiss();
            };

            $scope.save = function () {
                $modalInstance.close($scope.model.cancelReason);
            };

            $scope.validate = function () {
                $scope.disableSave = $scope.model.cancelReason.length <= 1;
            };
        }
    ]);
})();