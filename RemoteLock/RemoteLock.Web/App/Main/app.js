﻿(function () {
    'use strict';
    
    var app = angular.module('app', [
        'ngAnimate',
        'ngSanitize',

        'ui.router',
        'ui.bootstrap',
        'ui.jq',

        'abp',
        'daypilot',
        'ngCsvImport'
    ]);

    //Configuration for Angular UI routing.
    app.config([
        '$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');

            if (abp.auth.hasPermission('Pages.Tenants')) {
                $stateProvider
                    .state('tenants', {
                        url: '/tenants',
                        templateUrl: '/App/Main/views/tenants/index.cshtml',
                        menu: 'Tenants' //Matches to name of 'Tenants' menu in RemoteLockNavigationProvider
                    });
                $urlRouterProvider.otherwise('/tenants');
            }

            var pages = false;

            if (abp.auth.hasPermission('Pages')) {
                $stateProvider
                    .state('schedule', {
                        url: '/',
                        templateUrl: '/App/Main/views/schedule/index.cshtml',
                        menu: 'Schedule' //Matches to name of 'Schedule' menu in RemoteLockNavigationProvider
                    });

                pages = true;
            }

            if (abp.auth.hasPermission('RoleManagement')) {
                $stateProvider
                    .state('roles', {
                        url: '/roles',
                        templateUrl: '/App/Main/views/roles/index.cshtml',
                        menu: 'roles' //Matches to name of 'roles' menu in RemoteLockNavigationProvider
                    });
            }

            if (abp.auth.hasAnyOfPermissions('Management','Settings')) {
                $stateProvider
                    .state('settings', {
                        url: '/settings',
                        templateUrl: '/App/Main/views/settings/index.cshtml',
                        menu: 'Settings' //Matches to name of 'Settings' menu in RemoteLockNavigationProvider
                    });
            }

            if (abp.auth.hasPermission('Management')) {
                $stateProvider.state('rooms', {
                        url: '/rooms',
                        templateUrl: '/App/Main/views/rooms/index.cshtml',
                        menu: 'Rooms' //Matches to name of 'Rooms' menu in RemoteLockNavigationProvider
                    })
                    .state('users', {
                        url: '/users',
                        templateUrl: '/App/Main/views/users/index.cshtml',
                        menu: 'Users' //Matches to name of 'Users' menu in RemoteLockNavigationProvider
                    });
            }

            if (abp.auth.hasPermission('Reports')) {
                $stateProvider
                    .state('reports_schedules_company', {
                        url: '/reports_schedules_company',
                        templateUrl: '/App/Main/views/reports/schedules_company/index.cshtml',
                        menu: 'reports_schedules_company' //Matches to name of 'reports_schedules_company' menu in RemoteLockNavigationProvider
                    })
                    .state('reports_schedules_room', {
                        url: '/reports_schedules_room',
                        templateUrl: '/App/Main/views/reports/schedules_room/index.cshtml',
                        menu: 'reports_schedules_room' //Matches to name of 'schedules_room' menu in RemoteLockNavigationProvider
                    })
                    .state('Reports_Room_Codes', {
                        url: '/Reports_Room_Codes',
                        templateUrl: '/App/Main/views/reports/room_codes/index.cshtml',
                        menu: 'Reports_Room_Codes' //Matches to name of 'Reports_Schedules_Room_category' menu in RemoteLockNavigationProvider
                    });
            }
        }
    ]);
})();