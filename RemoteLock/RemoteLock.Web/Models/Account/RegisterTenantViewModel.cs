using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using RemoteLock.MultiTenancy;
using RemoteLock.Users;

namespace RemoteLock.Web.Models.Account
{
    public class RegisterTenantViewModel : IInputDto
    {
        [Required]
        [StringLength(Tenant.MaxTenancyNameLength)]
        public string TenancyName { get; set; }

        [Required]
        [StringLength(User.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(User.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(User.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        [StringLength(User.MaxPlainPasswordLength)]
        public string Password { get; set; }

        [StringLength(User.MaxPlainPasswordLength)]
        [Compare("Password", ErrorMessage = "ConfirmPasswordError")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string bossCreationKey { get; set; }

        [Required]
        public bool IsORM { get; set; }
    }
}