namespace RemoteLock.Web.Models.Account
{
    public class PasswordResetTemplateModel
    {
        public string PasswordResetLink { get; set; }

        public string DisplayName { get; set; }
    }
}