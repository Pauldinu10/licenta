using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using RemoteLock.MultiTenancy;
using RemoteLock.Users;

namespace RemoteLock.Web.Models.Account
{
    public class PasswordResetViewModel : IInputDto
    {
        /// <summary>
        /// Not required for single-tenant applications.
        /// </summary>
        [StringLength(Tenant.MaxTenancyNameLength)]
        public string TenancyName { get; set; }

        [StringLength(User.MaxUserNameLength)]
        public string EmailAddress { get; set; }
    }
}