using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using RemoteLock.Users;

namespace RemoteLock.Web.Models.Account
{
    public class PasswordConfirmViewModel : IInputDto
    {
        /// <summary>
        /// Not required for single-tenant applications.
        /// </summary>
        [StringLength(User.MaxPlainPasswordLength)]
        public string Password { get; set; }

        [StringLength(User.MaxPlainPasswordLength)]
        [Compare("Password", ErrorMessage = "ConfirmPasswordError")]
        public string ConfirmPassword { get; set; }
        public string Username { get; set; }
        public string PasswordResetCode { get; set; }
    }
}