﻿using System.Reflection;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Abp.Hangfire;
using Abp.Hangfire.Configuration;
using Abp.Localization;
using Abp.Modules;
using Abp.Web.Mvc;
using Abp.Web.SignalR;
using Hangfire;
using RemoteLock.Api;

namespace RemoteLock.Web
{
    [DependsOn(
        typeof(RemoteLockDataModule),
        typeof(RemoteLockApplicationModule),
        typeof(RemoteLockWebApiModule),
        typeof(AbpWebSignalRModule),
        typeof(AbpHangfireModule),
        typeof(AbpWebMvcModule))]
    public class RemoteLockWebModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Add/remove languages for your application
            Configuration.Localization.Languages.Add(new LanguageInfo("en", "English", "famfamfam-flag-england", false));
            Configuration.Localization.Languages.Add(new LanguageInfo("ro-RO", "Romana", "famfamfam-flag-ro", true));

            ////Enable database based localization
            //Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();

            //Configure navigation/menu
            Configuration.Navigation.Providers.Add<RemoteLockNavigationProvider>();

            //Configure Hangfire
            /*Configuration.BackgroundJobs.UseHangfire(configuration =>
            {
                configuration.GlobalConfiguration.UseSqlServerStorage(
                "Default"
                );
            });*/
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
