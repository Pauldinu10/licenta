﻿using Abp.Application.Navigation;
using Abp.Localization;
using RemoteLock.Authorization;

namespace RemoteLock.Web
{
    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class RemoteLockNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        "Schedule",
                        new LocalizableString("Menu_Schedule",
                            RemoteLockConsts.LocalizationSourceName),
                        url: "#/",
                        icon: "fa fa-calendar",
                        requiredPermissionName: PermissionNames.Pages
                        ))
                .AddItem(
                    new MenuItemDefinition(
                        "Tenants",
                        L("Tenants"),
                        url: "#/tenants",
                        icon: "fa fa-globe",
                        requiredPermissionName: PermissionNames.Pages_Tenants
                        )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "roles",
                        L("roles_roles"),
                        url: "#/roles",
                        icon: "fa fa-users",
                        requiredPermissionName: PermissionNames.RoleManagment
                        )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "Management",
                        new LocalizableString("Menu_Management",
                            RemoteLockConsts.LocalizationSourceName),
                        url: "#/manage",
                        icon: "fa fa-th",
                        requiredPermissionName: PermissionNames.Management
                        )
                        .AddItem(
                            new MenuItemDefinition(
                                "Rooms",
                                new LocalizableString("Menu_Rooms",
                                    RemoteLockConsts.LocalizationSourceName),
                                url: "#/rooms",
                                icon: "fa fa-bed",
                                requiredPermissionName: PermissionNames.Management
                                ))
                        .AddItem(
                            new MenuItemDefinition(
                                "Users",
                                new LocalizableString("Menu_RemoteUsers",
                                    RemoteLockConsts.LocalizationSourceName),
                                url: "#/users",
                                icon: "fa fa-users",
                                requiredPermissionName: PermissionNames.Management
                                ))
                )
                .AddItem(
                    new MenuItemDefinition(
                        "Report",
                        new LocalizableString("Menu_Reports",
                            RemoteLockConsts.LocalizationSourceName),
                        url: "#/rep",
                        requiredPermissionName: PermissionNames.Management,
                        icon: "fa fa-bar-chart"
                        )
                        .AddItem(
                            new MenuItemDefinition(
                                "reports_schedules_company",
                                new LocalizableString("Menu_Reports_Schedules_Fleet",
                                    RemoteLockConsts.LocalizationSourceName),
                                url: "#/reports_schedules_company",
                                requiredPermissionName: PermissionNames.Reports,
                                icon: "fa fa-database"
                            ))
                        .AddItem(
                            new MenuItemDefinition(
                                "reports_schedules_room",
                                new LocalizableString("Menu_Reports_Schedules_Room",
                                    RemoteLockConsts.LocalizationSourceName),
                                url: "#/reports_schedules_room",
                                icon: "fa fa-bed",
                                requiredPermissionName: PermissionNames.Reports
                            ))
                        .AddItem(
                            new MenuItemDefinition(
                                "Reports_Room_Codes",
                                new LocalizableString("Menu_Reports_Room_Codes",
                                    RemoteLockConsts.LocalizationSourceName),
                                url: "#/Reports_Room_Codes",
                                icon: "fa fa-key",
                                requiredPermissionName: PermissionNames.Reports
                            ))
                )
                .AddItem(
                    new MenuItemDefinition(
                        "Settigs",
                        new LocalizableString("Menu_Settings",
                            RemoteLockConsts.LocalizationSourceName),
                        url: "#/settings",
                        requiredPermissionName: PermissionNames.Management,
                        icon: "fa fa-cogs"
                        ))
                        ;
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, RemoteLockConsts.LocalizationSourceName);
        }
    }
}
