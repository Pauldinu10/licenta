﻿using Abp.Web.Mvc.Views;

namespace RemoteLock.Web.Views
{
    public abstract class RemoteLockWebViewPageBase : RemoteLockWebViewPageBase<dynamic>
    {

    }

    public abstract class RemoteLockWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected RemoteLockWebViewPageBase()
        {
            LocalizationSourceName = RemoteLockConsts.LocalizationSourceName;
        }
    }
}