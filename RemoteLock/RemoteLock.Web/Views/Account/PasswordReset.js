﻿(function () {

    $(function () {

        var DisplayList = function (list) {

            $("#LoginTenantSelection").css("display", "");
            $("#LoginInput").css("display", "none");

            var jq = $(".md-radio-list");
             for (var i = 0; i < list.length; i++) {
                 var tempalte =
  '<div class="md-radio"><input type="radio" class="md-radiobtn" id="TenancyName_@1" name="TenancyName" value="@1" @2><label for="TenancyName_@1">@1</label></div>';
                 tempalte = tempalte.replace("@2", i != 0 ? "" : "checked=\"checked\"");
                 jq.append($(tempalte.replace(new RegExp("@1", 'g'), list[i])));
            }
        }

        var displayResult = function (emailFound) {
            if (emailFound == true) {
                $("#LoginTenantSelection").css("display", "none");
                $("#LoginInput").css("display", "none");
                $("#Success").css("display", "");

                $("#ResultMessage").text($("#ResultMessage").data("loc").replace("{0}", $('#EmailAddress').val()));
                return true;
            }

            return false;
        }

        $('#ResetButton').click(function (e) {
            e.preventDefault();
            abp.ui.setBusy(
                $('#LoginArea'),
                abp.ajax({
                    url: abp.appPath + 'Account/PasswordReset',
                    type: 'POST',
                    data: JSON.stringify({
                        emailAddress: $('#EmailAddress').val()
                    }),
                    success: function (data) {
                        if (!displayResult(data.data))
                            DisplayList(data.data);
                    }
                })
            );
        });



        $('#ResetButtonTenant').click(function (e) {
            e.preventDefault();
            abp.ui.setBusy(
                $('#LoginArea'),
                abp.ajax({
                    url: abp.appPath + 'Account/PasswordReset',
                    type: 'POST',
                    data: JSON.stringify({
                        tenancyName: $('input[name="TenancyName"]:checked').val(),
                        emailAddress: $('#EmailAddress').val()
                    }),
                    success: function (data) {
                        displayResult(data.data);
                    }
                })
            );
        });

        $('#CancelButton').click(function (e) {
            e.preventDefault();
            $('.md-radio').remove();
            $("#LoginTenantSelection").css("display", "none");
            $("#LoginInput").css("display", "");
        });
    });

})();