﻿(function () {

    $(function () {

        var DisplayList = function (list) {

            $("#LoginTenantSelection").css("display", "");
            $("#LoginInput").css("display", "none");

            var jq = $(".md-radio-list");
             for (var i = 0; i < list.length; i++) {
                 var tempalte =
  '<div class="md-radio"><input type="radio" class="md-radiobtn" id="TenancyName_@1" name="TenancyName" value="@1" @2><label for="TenancyName_@1">@1</label></div>';
                 tempalte = tempalte.replace("@2", i != 0 ? "" : "checked=\"checked\"");
                 jq.append($(tempalte.replace(new RegExp("@1", 'g'), list[i])));
            }
        }

        $('#LoginButton').click(function (e) {
            e.preventDefault();
            abp.ui.setBusy(
                $('#LoginArea'),
                abp.ajax({
                    url: abp.appPath + 'Account/Login',
                    type: 'POST',
                    data: JSON.stringify({
                        tenancyName: $('#TenancyName').val(),
                        usernameOrEmailAddress: $('#EmailAddressInput').val(),
                        password: $('#PasswordInput').val(),
                        rememberMe: $('#RememberMeInput').is(':checked'),
                        returnUrlHash: $('#ReturnUrlHash').val()
                    }),
                    success: function (data) {

                        if (data)
                            DisplayList(data);
                    }
                })
            );
        });

        $('#LoginButtonTenant').click(function (e) {
            e.preventDefault();
            abp.ui.setBusy(
                $('#LoginArea'),
                abp.ajax({
                    url: abp.appPath + 'Account/Login',
                    type: 'POST',
                    data: JSON.stringify({
                        tenancyName: $('input[name="TenancyName"]:checked').val(),
                        usernameOrEmailAddress: $('#EmailAddressInput').val(),
                        password: $('#PasswordInput').val(),
                        rememberMe: $('#RememberMeInput').is(':checked'),
                        returnUrlHash: $('#ReturnUrlHash').val()
                    })
                })
            );
        });

        $('#CancelButton').click(function (e) {
            e.preventDefault();
            $('.md-radio').remove()
            $("#LoginTenantSelection").css("display", "none");
            $("#LoginInput").css("display", "");
        });

       
        $('a.social-login-link').click(function () {
            var $a = $(this);
            var $form = $a.closest('form');
            $form.find('input[name=provider]').val($a.attr('data-provider'));
            $form.submit();
        });

        $('#ReturnUrlHash').val(location.hash);

        $('#LoginForm input:first-child').focus();
    });

})();