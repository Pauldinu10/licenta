﻿using System;
using Abp.Application.Services.Dto;

namespace RemoteLock.Settings.Dto
{
    public class CompanyDto : IInputDto
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}