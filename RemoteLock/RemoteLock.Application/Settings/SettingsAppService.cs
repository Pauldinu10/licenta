using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Configuration;
using RemoteLock.Authorization;
using RemoteLock.Settings.Dto;

namespace RemoteLock.Settings
{
    [AbpAuthorize]
    public class SettingsAppService : RemoteLockAppServiceBase, ISettingsAppService
    {
        private readonly ISettingManager _settingManager;

        public SettingsAppService( ISettingManager settingManager)
        {
            _settingManager = settingManager;
        }

        [AbpAuthorize(PermissionNames.Management)]
        public async Task<CompanyDto> GetSettings()
        {
            return await GetCompanyDto(_settingManager, AbpSession.TenantId.Value);
        }

        [AbpAuthorize(PermissionNames.Settings)]
        public async Task UpdateSettings(CompanyDto input)
        {
            await _settingManager.ChangeSettingForTenantAsync(AbpSession.TenantId.Value,CoreSettingsProvider.SettingCompanyName,input.Name );
            await _settingManager.ChangeSettingForTenantAsync(AbpSession.TenantId.Value,CoreSettingsProvider.SettingCompanyPhone, input.Phone);
            await _settingManager.ChangeSettingForTenantAsync(AbpSession.TenantId.Value,CoreSettingsProvider.SettingCompanyEmail, input.Email);
        }

        public static async Task<CompanyDto> GetCompanyDto(ISettingManager settingManager, int tenantId)
        {
            var result = new CompanyDto();

            result.Name = await settingManager.GetSettingValueForTenantAsync(CoreSettingsProvider.SettingCompanyName, tenantId);
            result.Phone = await settingManager.GetSettingValueForTenantAsync(CoreSettingsProvider.SettingCompanyPhone, tenantId);
            result.Email = await settingManager.GetSettingValueForTenantAsync(CoreSettingsProvider.SettingCompanyEmail, tenantId);

            return result;
        }
    }
}