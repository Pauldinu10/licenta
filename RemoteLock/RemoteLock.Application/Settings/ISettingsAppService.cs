using System.Threading.Tasks;
using Abp.Application.Services;
using RemoteLock.Settings.Dto;

namespace RemoteLock.Settings
{
    public interface ISettingsAppService : IApplicationService
    {
        Task UpdateSettings(CompanyDto input);
        Task<CompanyDto> GetSettings();
    }
}