﻿using System.Collections.Generic;

namespace RemoteLock.Roles.Dto
{
    public class RolesListDto
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public List<string> GrantedPermissionNames { get; set; }
    }
}