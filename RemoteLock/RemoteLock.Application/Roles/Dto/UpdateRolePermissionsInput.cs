﻿using Abp.Application.Services.Dto;

namespace RemoteLock.Roles.Dto
{
    public class UpdateRolePermissionsInput : IInputDto
    {
        public int RoleId { get; set; }
        public string Permission { get; set; }
        public bool Grants { get; set; }
    }
}