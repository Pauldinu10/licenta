﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using RemoteLock.Authorization;
using RemoteLock.Authorization.Roles;
using RemoteLock.Roles.Dto;

namespace RemoteLock.Roles
{
    public class RoleAppService : RemoteLockAppServiceBase,IRoleAppService
    {
        private readonly RoleManager _roleManager;
        private readonly IPermissionManager _permissionManager;

        public RoleAppService(RoleManager roleManager, IPermissionManager permissionManager)
        {
            _roleManager = roleManager;
            _permissionManager = permissionManager;
        }
        [AbpAuthorize(PermissionNames.RoleManagment)]
        public async Task<ListResultOutput<RolesListDto>> GetRoles()
        {
            var roles =  _roleManager.Roles.Select(item=>new RolesListDto()
            {
                RoleId = item.Id,
                RoleName = item.DisplayName,
                GrantedPermissionNames = item.Permissions.Select(p=>p.Name).ToList()
            }).ToList();

            return new ListResultOutput<RolesListDto>(roles);
        }

        [AbpAuthorize(PermissionNames.RoleManagment)]
        public async Task UpdateRolePermissions(UpdateRolePermissionsInput input)
        {
            var role =await _roleManager.GetRoleByIdAsync(input.RoleId);

            if (role == null) return;

            var permission = _permissionManager.GetPermissionOrNull(input.Permission);

            if (permission == null) return;

            if (input.Grants)
                await _roleManager.GrantPermissionAsync(role, permission);
            else
                await _roleManager.ProhibitPermissionAsync(role, permission);
        }
    }
}