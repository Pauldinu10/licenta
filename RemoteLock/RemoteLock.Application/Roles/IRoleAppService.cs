﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RemoteLock.Roles.Dto;

namespace RemoteLock.Roles
{
    public interface IRoleAppService : IApplicationService
    {
        Task<ListResultOutput<RolesListDto>> GetRoles();
        Task UpdateRolePermissions(UpdateRolePermissionsInput input);
    }
}
