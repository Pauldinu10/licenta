using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RemoteLock.Schedule.Dto;

namespace RemoteLock.Schedule
{
    public interface IScheduleAppService : IApplicationService
    {
        Task<ListResultOutput<ScheduleListReportDto>> GetSchedules(SchedulesReportInput input);
        Task Create(CreateScheduleInput input);
        Task<bool> CancelSchedule(CancelScheduleInput input);
        Task<ListResultOutput<Guid>> GetAvailableRoomsForScheduleChange(GetAvailableRoomInput input);
        Task<bool> ChangeSchedule(ChangeScheduleInput input);

        Task<string> SendCodesToEmail(string scheduleId);
    }

}