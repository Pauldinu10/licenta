using Abp.Application.Services.Dto;

namespace RemoteLock.Schedule.Dto
{
    public class CreateScheduleInput : IInputDto
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string Notes { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string RoomId { get; set; }
    }
}