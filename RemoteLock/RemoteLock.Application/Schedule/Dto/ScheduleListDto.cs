﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace RemoteLock.Schedule.Dto
{

    [AutoMapFrom(typeof(Schedule))]
    public class ScheduleListDto : FullAuditedEntityDto<Guid>
    {
        public string Text { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string BarColor { get; set; }
        public string BackColor { get; set; }
        public string BorderColor { get; set; }
        public string FontColor { get; set; }
        public string BubbleHtml { get; set; }
        public string Resource { get; set; }
    }
}