using Abp.Application.Services.Dto;

namespace RemoteLock.Schedule.Dto
{
    public class ChangeScheduleInput : IInputDto
    {
        public string ScheduleId { get; set; }
        public string NewRoomId { get; set; }
    }
}