using Abp.Application.Services.Dto;

namespace RemoteLock.Schedule.Dto
{
    public class CancelScheduleInput : IInputDto
    {
        public string ScheduldId { get; set; }
        public bool ReassingDifferent { get; set; }
        public string CancelReason { get; set; }
    }
}