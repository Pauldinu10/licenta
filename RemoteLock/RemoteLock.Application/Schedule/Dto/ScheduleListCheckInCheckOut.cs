using System;

namespace RemoteLock.Schedule.Dto
{
    public class ScheduleCheckInCheckOut
    {
        public bool CanDoCheckIn { get;  set; }
        public bool CanDoCheckOut { get;  set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string LicensePlate { get; set; }
        public Guid ScheduleId { get; set; }
        public int FuelingNeeded { get; set; }
    }
}