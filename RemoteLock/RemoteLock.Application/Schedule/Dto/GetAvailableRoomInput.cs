using Abp.Application.Services.Dto;

namespace RemoteLock.Schedule.Dto
{
    public class GetAvailableRoomInput : IInputDto
    {
        public string Start { get; set; }
        public string End { get; set; }
    }
}