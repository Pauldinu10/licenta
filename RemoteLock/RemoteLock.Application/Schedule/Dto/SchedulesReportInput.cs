using Abp.Application.Services.Dto;

namespace RemoteLock.Schedule.Dto
{
    public class SchedulesReportInput : IInputDto
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string RoomId { get; set; }
        public bool ShowCanceled { get; set; }
    }
}