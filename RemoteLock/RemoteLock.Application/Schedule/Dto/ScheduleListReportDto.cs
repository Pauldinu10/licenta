﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace RemoteLock.Schedule.Dto
{
    [AutoMapFrom(typeof (Schedule))]
    public class ScheduleListReportDto : FullAuditedEntityDto<Guid>
    {
        public DateTime Start { get;  set; }
        public DateTime End { get;  set; }
        public string Notes { get; set; }
        public string Text { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public virtual Guid? Resource { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? CreatedBy  { get; set; }
        public bool Canceled { get; set; }
        public string CancelReason { get; set; }
    }
}