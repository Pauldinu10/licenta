using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Notifications;
using Abp.Runtime.Session;
using RemoteLock.Authorization;
using RemoteLock.EmailSender;
using RemoteLock.Room;
using RemoteLock.Schedule.Dto;
using RemoteLock.Settings;

namespace RemoteLock.Schedule
{
    [AbpAuthorize]
    public class ScheduleAppService : RemoteLockAppServiceBase, IScheduleAppService
    {
        public static string OwnColor = "green";
        public static string BookedColor = "#F5C4C4";

        private readonly IScheduleManager _scheduleManager;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IRepository<Room.Room, Guid> _roomRepository;
        private readonly ISettingManager _settingManager;

        public ScheduleAppService(
            IScheduleManager scheduleManager,
            INotificationSubscriptionManager notificationSubscriptionManager,
            IRepository<Room.Room, Guid> roomRepository,
            ISettingManager settingManager)
        {
            _scheduleManager = scheduleManager;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _roomRepository = roomRepository;
            _settingManager = settingManager;
        }
        [AbpAuthorize(PermissionNames.Pages)]
        public async Task<ListResultOutput<ScheduleListReportDto>> GetSchedules(SchedulesReportInput model)
        {
            var startDate = DateTime.Parse(model.Start);
            var endDate = DateTime.Parse(model.End);

            List<Schedule> schedules = await _scheduleManager.GetAllAsync(startDate, endDate);

            if (!model.ShowCanceled)
                schedules = schedules.Where(item => !item.Canceled).ToList();

            if (!string.IsNullOrWhiteSpace(model.RoomId))
            {
                var roomId = Guid.Parse(model.RoomId);
                schedules = schedules.Where(item => item.RoomId == roomId).ToList();
            }


            var result = schedules.Select(item => new ScheduleListReportDto
            {
                CreatedBy = item.CreatorUserId,
                CreatedDate = item.CreationTime,
                Notes = item.Description,
                Text = item.Name??item.Email??item.Phone??item.Description,
                Name = item.Name,
                Phone = item.Phone,
                Email = item.Email,
                Start = item.Start,
                End = item.End,
                Resource = item.RoomId,
                Id = item.Id,
                Canceled = item.Canceled,
                CancelReason = item.CancelReason
            }).ToList();

            return new ListResultOutput<ScheduleListReportDto>(result);
        }
        [AbpAuthorize(PermissionNames.Pages)]
        public async Task Create(CreateScheduleInput input)
        {
            var startDate = DateTime.Parse(input.Start);
            var endDate = DateTime.Parse(input.End);

            var tenantId = AbpSession.GetTenantId();

            var roomId = Guid.Parse(input.RoomId);

            var schedule = Schedule.Create(tenantId
                , roomId
                , startDate
                , endDate
                , input.Name
                , input.Email
                , input.Notes
                , input.Phone);

            await _scheduleManager.CreateAsync(schedule);
        }

        [AbpAuthorize(PermissionNames.Management)]
        public async Task<bool> CancelSchedule(CancelScheduleInput model)
        {
          return await _scheduleManager.CancelSchedule(Guid.Parse(model.ScheduldId),model.CancelReason, model.ReassingDifferent);
        }

        [AbpAuthorize(PermissionNames.Pages)]
        public async Task<ListResultOutput<Guid>> GetAvailableRoomsForScheduleChange(GetAvailableRoomInput input)
        {
            var tenantId = AbpSession.GetTenantId();

            var startDate = DateTime.Parse(input.Start);
            var endDate = DateTime.Parse(input.End);

            return new ListResultOutput<Guid>((await
                    _scheduleManager.GetAvailableRoomForScheduleChange(tenantId, startDate, endDate)));
        }
        [AbpAuthorize(PermissionNames.Management)]
        public async Task<bool> ChangeSchedule(ChangeScheduleInput input)
        {
            var scheduleId = Guid.Parse(input.ScheduleId);
            return await _scheduleManager.ChangeSchedule(scheduleId, Guid.Parse(input.NewRoomId));
        }

        public async Task<string> SendCodesToEmail(string scheduleId)
        {
            var schId = Guid.Parse(scheduleId);

            var schedule = await _scheduleManager.GetAsync(schId);

            if (!schedule.RoomId.HasValue)
            {
                return "";
            }

            var room = await _roomRepository.GetAsync(schedule.RoomId.Value);

            var codesRegion = "";

            var companyInfo= await SettingsAppService.GetCompanyDto(_settingManager, AbpSession.TenantId.Value);

            var currentDay = schedule.Start;

            RemoteLockConsts.EESTTimeZoneInfo.IsDaylightSavingTime(currentDay);

            while (currentDay < schedule.End)
            {
                var start = currentDay.AddHours(RemoteLockConsts.EESTTimeZoneInfo.IsDaylightSavingTime(currentDay) ? 13 : 14);
                var end = currentDay.AddHours(RemoteLockConsts.EESTTimeZoneInfo.IsDaylightSavingTime(currentDay.AddDays(1)) ? 37 : 38);

                codesRegion += string.Format(L("codes_email_code_template"), start.ToString("g"), end.ToString("g"), RoomSecurity.GetCode(room.DeviceId, currentDay));

                currentDay = currentDay.AddDays(1);
            }

            EmailSender.EmailSender.SendEmail(new EmailModel
            {
                Subject = string.Format(L("codes_email_subject"),companyInfo.Name),
                Body = string.Format(L("codes_email_body"), 
                room.Name, 
                room.Address, 
                schedule.Start.ToString("d"), 
                schedule.End.ToString("d"),
                codesRegion,
                companyInfo.Phone,
                companyInfo.Email),
                IsBodyHtml = false,
                EmailAddress = schedule.Email
            });

            return schedule.Email;
        }
    }
}