﻿using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;

namespace RemoteLock
{
    [DependsOn(typeof(RemoteLockCoreModule), typeof(AbpAutoMapperModule))]
    public class RemoteLockApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
