using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using RemoteLock.Authorization;
using RemoteLock.Authorization.Roles;
using RemoteLock.Users.Dto;

namespace RemoteLock.Users
{
    public class UserAppService : RemoteLockAppServiceBase, IUserAppService
    {
        private readonly UserManager _userManager;
        private readonly IPermissionManager _permissionManager;
        private readonly IRepository<User, long> _userRepository;

        public UserAppService(UserManager userManager, IPermissionManager permissionManager,
            IRepository<User, long> userRepository)
        {
            _userManager = userManager;
            _permissionManager = permissionManager;
            _userRepository = userRepository;
        }
        [AbpAuthorize(PermissionNames.Management)]
        public async Task ProhibitPermission(ProhibitPermissionInput input)
        {
            var user = await _userManager.GetUserByIdAsync(input.UserId);
            var permission = _permissionManager.GetPermission(input.PermissionName);

            await _userManager.ProhibitPermissionAsync(user, permission);
        }

        [AbpAuthorize(PermissionNames.Management)]
        public async Task ToggleUserSuspended(long userId)
        {
            await _userManager.UserStore.ToggleUserSuspendedState(userId);
        }
        [AbpAuthorize(PermissionNames.Management)]
        public async Task Add(CreateUserInput model)
        {
            var user =
                await
                    _userManager.UserStore.AddUser(AbpSession.GetTenantId(), model.Name, model.Surname,
                        model.EmailAddress,
                        model.Password, model.Username, model.UserType);

            if (model.IsManager)
                await _userManager.AddToRoleAsync(user,
                    model.IsORM ? StaticRoleNames.Tenants.ORMManager : StaticRoleNames.Tenants.Manager);
            else
                await _userManager.AddToRoleAsync(user,
                    model.IsORM ? StaticRoleNames.Tenants.ORMMember : StaticRoleNames.Tenants.Member);
        }
        [AbpAuthorize(PermissionNames.Management)]
        public async Task Update(UpdateUserInput model)
        {
            var user = await _userManager.UserStore.UpdateUser(model.Id, model.Name, model.Surname, model.EmailAddress, model.UserType);

            if (model.IsManager)
            {
                await _userManager.RemoveFromRoleAsync(user.Id,
                    model.IsORM ? StaticRoleNames.Tenants.ORMMember : StaticRoleNames.Tenants.Member);

                await _userManager.AddToRoleAsync(user.Id,
                    model.IsORM ? StaticRoleNames.Tenants.ORMManager : StaticRoleNames.Tenants.Manager);}
            else
            {
                await _userManager.RemoveFromRoleAsync(user.Id,
                    model.IsORM ? StaticRoleNames.Tenants.ORMManager : StaticRoleNames.Tenants.Manager);

                await _userManager.AddToRoleAsync(user.Id,
                    model.IsORM ? StaticRoleNames.Tenants.ORMMember : StaticRoleNames.Tenants.Member);
            }
        }

        [AbpAuthorize(PermissionNames.Management)]
        public async Task RemoveFromRole(long userId, string roleName)
        {
            CheckErrors(await _userManager.RemoveFromRoleAsync(userId, roleName));
        }

        public async Task<ListResultOutput<UserListDto>> GetList()
        {
            var users = await _userRepository.GetAllListAsync();

            var list =
                new ListResultOutput<UserListDto>(
                    users.Where(item=>item.UserName != User.bossUserUsername).OrderBy(item => item.Name).ThenBy(item => item.Surname).MapTo<List<UserListDto>>());

            foreach(var item in list.Items)
                item.IsManager = await _userManager.IsInRoleAsync(item.Id, StaticRoleNames.Tenants.Manager) ||
                                 await _userManager.IsInRoleAsync(item.Id, StaticRoleNames.Tenants.ORMManager);

            return list;
        }

        public async Task<bool> ChangePassword(PasswordChangeInput model )
        {
            return
                await
                    _userManager.UserStore.ChangePassword(AbpSession.GetUserId(), model.OldPassword, model.NewPassword);
        }

        [AbpAuthorize(PermissionNames.Management)]
        public async Task ResetPassword(long userId)
        {
            await _userManager.UserStore.ResetPassword(userId);
        }
    }
}