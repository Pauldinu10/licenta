using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RemoteLock.Users.Dto;

namespace RemoteLock.Users
{
    public interface IUserAppService : IApplicationService
    {
        Task ProhibitPermission(ProhibitPermissionInput input);
        Task RemoveFromRole(long userId, string roleName);
        Task<ListResultOutput<UserListDto>> GetList();
        Task ToggleUserSuspended(long userId);
        Task Add(CreateUserInput model);

        Task Update(UpdateUserInput model);

        Task<bool> ChangePassword(PasswordChangeInput model);
        Task ResetPassword(long userId);
    }
}