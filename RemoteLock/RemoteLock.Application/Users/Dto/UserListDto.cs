using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace RemoteLock.Users.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserListDto : FullAuditedEntityDto<long>
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public bool IsActive { get; set; }
        public bool IsManager { get; set; }
        public string Username { get; set; }
        public int? UserType { get; set; }
    }
}