using Abp.Application.Services.Dto;

namespace RemoteLock.Users.Dto
{
    public class CreateUserInput : IInputDto
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public bool IsManager { get; set; }

        public bool IsORM { get; set; }
        public string Username { get; set; }
        public int? UserType { get; set; }
    }
}