using Abp.Application.Services.Dto;

namespace RemoteLock.Users.Dto
{
    public class PasswordChangeInput : IInputDto
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}