using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RemoteLock.Rooms.Dto;

namespace RemoteLock.Rooms
{
    public interface IRoomAppService : IApplicationService
    {
        Task<RoomListDto> Get(string id);
        Task Delete(string id);
        Task<ListResultOutput<RoomListDto>> GetList();
        Task Create(CreateRoomInput input);
        Task SetRoomState(string id, int roomState);
        Task Update(UpdateRoomInput input);
        Task<RoomCodes> GetTodayCodes(string id);
        Task<Dictionary<DateTime, string>> GetCodeForSchedule(string scheduleId);
        Task<Dictionary<DateTime, string>> GetCodesForRoom(GetRoomCodesInput input);
    }
}