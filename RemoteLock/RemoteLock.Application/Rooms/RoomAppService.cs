using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using RemoteLock.Authorization;
using RemoteLock.Room;
using RemoteLock.Rooms.Dto;

namespace RemoteLock.Rooms
{
    [AbpAuthorize]
    public class RoomAppService : RemoteLockAppServiceBase, IRoomAppService
    {
        private string namef;
        private readonly IRepository<Room.Room, Guid> _roomRepository;
        private readonly IRepository<Schedule.Schedule, Guid> _scheduleRepository;
        private readonly IRepository<Users.User, long> _userRepository;

        public RoomAppService(
            IRepository<Room.Room, Guid> roomRepository,
            IRepository<Schedule.Schedule, Guid> scheduleRepository)
        {
            _roomRepository = roomRepository;
            _scheduleRepository = scheduleRepository;
        }

        public async Task<RoomListDto> Get(string id)
        {
            var room = await _roomRepository.GetAsync(Guid.Parse(id));

            return room.MapTo<RoomListDto>();
        }

        public async Task Delete(string id)
        {
            var guidId = Guid.Parse(id);

            var room = await _roomRepository.FirstOrDefaultAsync(guidId);

            var getAllSchedules = _scheduleRepository.GetAllList(item => item.RoomId == guidId);

            foreach (var sch in getAllSchedules)
            {
                sch.RoomId = null;
                await _scheduleRepository.UpdateAsync(sch);
            }

            await _roomRepository.DeleteAsync(room);
        }

        public async Task<ListResultOutput<RoomListDto>> GetList()
        {
            var rooms= await _roomRepository
                    .GetAllListAsync();

            return new ListResultOutput<RoomListDto>(rooms.OrderBy(item => item.Name).MapTo<List<RoomListDto>>());
        }
        [AbpAuthorize(PermissionNames.Management)]
        public async Task Create(CreateRoomInput input)
        {
            var room = Room.Room.Create(AbpSession.GetTenantId(), input.Name,input.Address, input.Description,input.DeviceId,input.AirbnbId, input.BookingId);
            await _roomRepository.InsertAsync(room);
        }

        [AbpAuthorize(PermissionNames.Management)]
        public async Task Update(UpdateRoomInput input)
        {
            var room = await _roomRepository.GetAsync(input.Id);

            room.Name = input.Name;
            room.Description = input.Description;

            if (room.DeviceId != input.DeviceId)
            {
                if (room.DeviceId == null)
                    room.BatteryChangedDate = DateTime.Now;

                room.DeviceId = input.DeviceId;
                room.InstallDate= DateTime.Now;
            }

            room.AirbnbId = input.AirbnbId;
            room.BookingId = input.BookingId;
            room.Address = input.Address;

            await _roomRepository.UpdateAsync(room);
        }

        public async Task SetRoomState(string id, int roomState)
        {
            var room = await _roomRepository.GetAsync(Guid.Parse(id));

            room.Status = roomState;

            await _roomRepository.UpdateAsync(room);
        }

        public async Task<RoomCodes> GetTodayCodes(string id)
        {
            var result = await Get(id);

            return new RoomCodes
            {
                MorningCode = RoomSecurity.GetCode(result.DeviceId, DateTime.Today.AddDays(-1)),
                EveningCode = RoomSecurity.GetCode(result.DeviceId, DateTime.Today)
            };
        }

        public async Task<Dictionary<DateTime,string>> GetCodeForSchedule(string scheduleId)
        {
            var schedule = await _scheduleRepository.GetAsync(Guid.Parse(scheduleId));

            var results = new Dictionary<DateTime, string>();


            if (!schedule.RoomId.HasValue)
                return results;

            var room = await _roomRepository.GetAsync(schedule.RoomId.Value);

            var currentDay = schedule.Start;


            while (currentDay < schedule.End)
            {
                results.Add(currentDay, RoomSecurity.GetCode(room.DeviceId, currentDay));

                currentDay = currentDay.AddDays(1);
            }

            return results;
        }

        [AbpAuthorize(PermissionNames.Management)]
        public async Task<Dictionary<DateTime, string>> GetCodesForRoom(GetRoomCodesInput input)
        {
            var room = await _roomRepository.GetAsync(Guid.Parse(input.RoomId));
            var user = await GetCurrentUserAsync();


            var currentDay = DateTime.Parse(input.Start);
            var endDay = DateTime.Parse(input.End);
            if (user.Name != "boss")
            {
                if (endDay > DateTime.Now.AddMonths(6))
                    throw new Exception("Access Codes connot be requested more than 6 months in advance.");
            }
            var results = new Dictionary<DateTime, string>();

            while (currentDay <= endDay)
            {
                results.Add(currentDay, RoomSecurity.GetCode(room.DeviceId, currentDay));
                currentDay = currentDay.AddDays(1);
            }

            return results;
            
        }

        
    }
}