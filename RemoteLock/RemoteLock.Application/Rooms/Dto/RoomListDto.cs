using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace RemoteLock.Rooms.Dto
{
    [AutoMapFrom(typeof(Room.Room))]
    public class RoomListDto : FullAuditedEntityDto<Guid>
    {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public string DeviceId { get; set; }
        public string AirbnbId { get; set; }
        public string BookingId { get; set; }
        public string SyncDateTimeAirbnb { get; set; }
        public string SyncDateTimeBooking { get; set; }
        public string InstallDate { get; set; }
        public string BatteryChangedDate { get; set; }
        public Guid? LocationId { get; set; }
        public string Address { get; set; }

    }
}