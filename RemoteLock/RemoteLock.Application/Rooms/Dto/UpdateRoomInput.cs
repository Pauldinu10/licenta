using System;
using Abp.Application.Services.Dto;

namespace RemoteLock.Rooms.Dto
{
    public class UpdateRoomInput : FullAuditedEntityDto<Guid>
    {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public string DeviceId { get; set; }
        public string AirbnbId { get; set; }
        public string BookingId { get; set; }
        public string Address { get; set; }
    }
}