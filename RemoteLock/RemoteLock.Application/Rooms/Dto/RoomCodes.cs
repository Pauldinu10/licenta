namespace RemoteLock.Rooms.Dto
{
    public class RoomCodes
    {
        public string MorningCode { get; set; }
        public string EveningCode { get; set; }
    }
}