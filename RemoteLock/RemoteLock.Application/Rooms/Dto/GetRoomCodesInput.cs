using Abp.Application.Services.Dto;

namespace RemoteLock.Rooms.Dto
{
    public class GetRoomCodesInput : IInputDto
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string RoomId { get; set; }
    }
}