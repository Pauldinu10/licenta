﻿using System.Threading.Tasks;
using Abp.Application.Services;
using RemoteLock.Sessions.Dto;

namespace RemoteLock.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
